#pragma bank 255

// Scene: Choose Gender - Wizard
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_24.h"
#include "data/spritesheet_25.h"
#include "data/spritesheet_26.h"
#include "data/spritesheet_13.h"

BANKREF(scene_8_sprites)

const far_ptr_t scene_8_sprites[] = {
    TO_FAR_PTR_T(spritesheet_24),
    TO_FAR_PTR_T(spritesheet_25),
    TO_FAR_PTR_T(spritesheet_26),
    TO_FAR_PTR_T(spritesheet_13)
};
