.module script_rollunder

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255


___bank_script_rollunder = 255
.globl ___bank_script_rollunder
.CURRENT_SCRIPT_BANK == ___bank_script_rollunder

_script_rollunder::
        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 20
        VM_SET_INDIRECT         ^/(.ARG2 - 1)/, .ARG0
        VM_POP                  1

        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2)/
        VM_IF_CONST .EQ         .ARG0, 1, 1$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2)/
        VM_IF_CONST .EQ         .ARG0, 20, 3$, 1
        ; If Variable .LTE Variable
        VM_PUSH_VALUE_IND       ^/(.ARG3)/
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 1)/
        VM_IF .LTE              .ARG0, .ARG1, 5$, 2
        ; Variable Set To False
        VM_PUSH_CONST           0
        VM_SET_INDIRECT         ^/(.ARG4 - 1)/, .ARG0
        VM_POP                  1

        VM_JUMP                 6$
5$:
        ; Variable Set To True
        VM_PUSH_CONST           1
        VM_SET_INDIRECT         ^/(.ARG4 - 1)/, .ARG0
        VM_POP                  1

6$:

        VM_JUMP                 4$
3$:
        ; Variable Set To False
        VM_PUSH_CONST           0
        VM_SET_INDIRECT         ^/(.ARG4 - 1)/, .ARG0
        VM_POP                  1

4$:

        VM_JUMP                 2$
1$:
        ; Variable Set To True
        VM_PUSH_CONST           1
        VM_SET_INDIRECT         ^/(.ARG4 - 1)/, .ARG0
        VM_POP                  1

2$:

        VM_RET_FAR
