#pragma bank 255

// Scene: D1_R2

#include "gbs_types.h"
#include "data/background_15.h"
#include "data/scene_15_collisions.h"
#include "data/palette_3.h"
#include "data/palette_10.h"
#include "data/spritesheet_4.h"
#include "data/scene_15_actors.h"
#include "data/scene_15_triggers.h"
#include "data/scene_15_sprites.h"
#include "data/script_s15_init.h"

BANKREF(scene_15)

const struct scene_t scene_15 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_15),
    .collisions = TO_FAR_PTR_T(scene_15_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_3),
    .sprite_palette = TO_FAR_PTR_T(palette_10),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 4,
    .n_triggers = 2,
    .n_sprites = 2,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_15_actors),
    .triggers = TO_FAR_PTR_T(scene_15_triggers),
    .sprites = TO_FAR_PTR_T(scene_15_sprites),
    .script_init = TO_FAR_PTR_T(script_s15_init)
};
