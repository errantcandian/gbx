#pragma bank 255

// Scene: D1_R2
// Actors

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/script_s15a0_interact.h"
#include "data/spritesheet_29.h"
#include "data/script_s15a1_interact.h"
#include "data/spritesheet_29.h"
#include "data/script_s15a2_interact.h"
#include "data/spritesheet_33.h"

BANKREF(scene_15_actors)

const struct actor_t scene_15_actors[] = {
    {
        // DoorE,
        .pos = {
            .x = 144 * 16,
            .y = 72 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_LEFT,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s15a0_interact),
        .reserve_tiles = 0
    },
    {
        // DoorN,
        .pos = {
            .x = 72 * 16,
            .y = 8 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s15a1_interact),
        .reserve_tiles = 0
    },
    {
        // FalseDoor,
        .pos = {
            .x = 0 * 16,
            .y = 72 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_RIGHT,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s15a2_interact),
        .reserve_tiles = 0
    },
    {
        // Mimic,
        .pos = {
            .x = 24 * 16,
            .y = 72 * 16
        },
        .bounds = {
            .left = -8,
            .bottom = 7,
            .right = 15,
            .top = 0
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_33),
        .move_speed = 16,
        .anim_tick = 15,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .reserve_tiles = 0
    }
};
