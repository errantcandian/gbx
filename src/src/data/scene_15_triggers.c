#pragma bank 255

// Scene: D1_R2
// Triggers

#include "gbs_types.h"
#include "data/script_s15t0_interact.h"
#include "data/script_s15t1_interact.h"

BANKREF(scene_15_triggers)

const struct trigger_t scene_15_triggers[] = {
    {
        // Trigger 1,
        .x = 19,
        .y = 8,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s15t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 9,
        .y = 0,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s15t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
