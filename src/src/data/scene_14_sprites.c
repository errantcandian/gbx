#pragma bank 255

// Scene: D1_R1
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/spritesheet_31.h"
#include "data/spritesheet_32.h"

BANKREF(scene_14_sprites)

const far_ptr_t scene_14_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29),
    TO_FAR_PTR_T(spritesheet_31),
    TO_FAR_PTR_T(spritesheet_32)
};
