#ifndef SCENE_19_ACTORS_H
#define SCENE_19_ACTORS_H

// Scene: D1_R6
// Actors

#include "gbs_types.h"

BANKREF_EXTERN(scene_19_actors)
extern const struct actor_t scene_19_actors[];

#endif
