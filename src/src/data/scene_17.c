#pragma bank 255

// Scene: D1_R4

#include "gbs_types.h"
#include "data/background_17.h"
#include "data/scene_17_collisions.h"
#include "data/palette_4.h"
#include "data/palette_12.h"
#include "data/spritesheet_34.h"
#include "data/scene_17_actors.h"
#include "data/scene_17_triggers.h"
#include "data/scene_17_sprites.h"
#include "data/script_s17_init.h"

BANKREF(scene_17)

const struct scene_t scene_17 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_17),
    .collisions = TO_FAR_PTR_T(scene_17_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_4),
    .sprite_palette = TO_FAR_PTR_T(palette_12),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_34),
    .n_actors = 2,
    .n_triggers = 1,
    .n_sprites = 2,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_17_actors),
    .triggers = TO_FAR_PTR_T(scene_17_triggers),
    .sprites = TO_FAR_PTR_T(scene_17_sprites),
    .script_init = TO_FAR_PTR_T(script_s17_init)
};
