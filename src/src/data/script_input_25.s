.module script_input_25

.include "vm.i"
.include "data/game_globals.i"
.include "macro.i"

.globl _fade_frames_per_step, ___bank_scene_14, _scene_14, ___bank_scene_15, _scene_15, ___bank_scene_16, _scene_16, ___bank_scene_17, _scene_17, ___bank_scene_18, _scene_18, ___bank_scene_19, _scene_19, ___bank_scene_20, _scene_20, ___bank_scene_25, _scene_25, ___bank_scene_24, _scene_24, ___bank_scene_13, _scene_13

.area _CODE_255

ACTOR = -4

___bank_script_input_25 = 255
.globl ___bank_script_input_25
.CURRENT_SCRIPT_BANK == ___bank_script_input_25

_script_input_25::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_PREVIOUSROOM, 0, 1$, 0
        ; Switch Variable
        ; case 1:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 1, 3$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_14

        VM_JUMP                 19$
3$:
        ; case 2:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 2, 4$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_15

        VM_JUMP                 19$
4$:
        ; case 3:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 3, 5$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_16

        VM_JUMP                 19$
5$:
        ; case 4:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 4, 6$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_17

        VM_JUMP                 19$
6$:
        ; case 5:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 5, 7$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_18

        VM_JUMP                 19$
7$:
        ; case 6:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 6, 8$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_19

        VM_JUMP                 19$
8$:
        ; case 7:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 7, 9$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_20

        VM_JUMP                 19$
9$:
        ; case 8:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 8, 10$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_25

        VM_JUMP                 19$
10$:
        ; case 9:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 9, 11$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_24

        VM_JUMP                 19$
11$:
        ; case 10:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 10, 12$, 0
        VM_JUMP                 19$
12$:
        ; case 11:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 11, 13$, 0
        VM_JUMP                 19$
13$:
        ; case 12:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 12, 14$, 0
        VM_JUMP                 19$
14$:
        ; case 13:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 13, 15$, 0
        VM_JUMP                 19$
15$:
        ; case 14:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 14, 16$, 0
        VM_JUMP                 19$
16$:
        ; case 15:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 15, 17$, 0
        VM_JUMP                 19$
17$:
        ; case 16:
        VM_IF_CONST .NE         VAR_PREVIOUSROOM, 16, 18$, 0
        VM_JUMP                 19$
18$:
        ; default:
19$:

        VM_JUMP                 2$
1$:
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_13

2$:

        ; Stop Script
        VM_STOP
