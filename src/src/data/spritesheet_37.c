#pragma bank 255
// SpriteSheet: Waterfall
  
#include "gbs_types.h"
#include "data/tileset_47.h"

BANKREF(spritesheet_37)

#define SPRITE_37_STATE_DEFAULT 0
#define SPRITE_37_STATE_SPECIAL_1 0

const metasprite_t spritesheet_37_metasprite_0[]  = {
    { 0, 8, 0, 3 }, { 0, -8, 2, 3 }, { -8, 8, 4, 3 }, { 0, -8, 6, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_37_metasprite_1[]  = {
    { 0, 8, 8, 3 }, { 0, -8, 10, 3 }, { -8, 8, 12, 3 }, { 0, -8, 14, 3 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_37_metasprites[] = {
    spritesheet_37_metasprite_0,
    spritesheet_37_metasprite_1
};

const struct animation_t spritesheet_37_animations[] = {
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    }
};

const UWORD spritesheet_37_animations_lookup[] = {
    SPRITE_37_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_37 = {
    .n_metasprites = 2,
    .emote_origin = {
        .x = 0,
        .y = -24
    },
    .metasprites = spritesheet_37_metasprites,
    .animations = spritesheet_37_animations,
    .animations_lookup = spritesheet_37_animations_lookup,
    .bounds = {
        .left = 8,
        .bottom = -16,
        .right = 8,
        .top = -16
    },
    .tileset = TO_FAR_PTR_T(tileset_47),
    .cgb_tileset = { NULL, NULL }
};
