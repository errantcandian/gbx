#pragma bank 255
// SpriteSheet: armour
  
#include "gbs_types.h"
#include "data/tileset_50.h"

BANKREF(spritesheet_40)

#define SPRITE_40_STATE_DEFAULT 0
#define SPRITE_40_STATE_SPECIAL_1 0

const metasprite_t spritesheet_40_metasprite_0[]  = {
    { 0, 8, 0, 0 }, { 0, -8, 2, 0 }, { 0, -8, 4, 0 }, { 0, -8, 6, 0 }, { 0, -8, 8, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_40_metasprite_1[]  = {
    { 0, 24, 10, 0 }, { 0, -8, 12, 0 }, { 0, -8, 0, 0 }, { 0, -8, 2, 0 }, { 0, -8, 14, 0 }, { 0, -8, 12, 0 }, { 0, -8, 16, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_40_metasprite_2[]  = {
    { 0, 8, 18, 0 }, { 0, -8, 20, 0 }, { 0, -8, 14, 0 }, { 0, -8, 0, 0 }, { 0, -8, 8, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_40_metasprite_3[]  = {
    { 0, 8, 12, 0 }, { 0, -8, 6, 0 }, { 0, -8, 14, 0 }, { 0, -8, 22, 0 }, { 0, -8, 24, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_40_metasprite_4[]  = {
    { 0, 8, 12, 0 }, { 0, -8, 2, 0 }, { 0, -8, 14, 0 }, { 0, -8, 6, 0 }, { 0, -8, 26, 0 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_40_metasprites[] = {
    spritesheet_40_metasprite_0,
    spritesheet_40_metasprite_1,
    spritesheet_40_metasprite_2,
    spritesheet_40_metasprite_3,
    spritesheet_40_metasprite_4
};

const struct animation_t spritesheet_40_animations[] = {
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    }
};

const UWORD spritesheet_40_animations_lookup[] = {
    SPRITE_40_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_40 = {
    .n_metasprites = 5,
    .emote_origin = {
        .x = 0,
        .y = -32
    },
    .metasprites = spritesheet_40_metasprites,
    .animations = spritesheet_40_animations,
    .animations_lookup = spritesheet_40_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_50),
    .cgb_tileset = { NULL, NULL }
};
