#ifndef SCENE_15_ACTORS_H
#define SCENE_15_ACTORS_H

// Scene: D1_R2
// Actors

#include "gbs_types.h"

BANKREF_EXTERN(scene_15_actors)
extern const struct actor_t scene_15_actors[];

#endif
