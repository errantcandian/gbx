#pragma bank 255

// Scene: Choose Class - Wizard

#include "gbs_types.h"
#include "data/background_3.h"
#include "data/scene_4_collisions.h"
#include "data/palette_0.h"
#include "data/palette_6.h"
#include "data/spritesheet_13.h"
#include "data/scene_4_actors.h"
#include "data/scene_4_triggers.h"
#include "data/scene_4_sprites.h"
#include "data/script_s4_init.h"

BANKREF(scene_4)

const struct scene_t scene_4 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_POINTNCLICK,
    .background = TO_FAR_PTR_T(background_3),
    .collisions = TO_FAR_PTR_T(scene_4_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_0),
    .sprite_palette = TO_FAR_PTR_T(palette_6),
    .reserve_tiles = 0,
    .player_sprite = TO_FAR_PTR_T(spritesheet_13),
    .n_actors = 6,
    .n_triggers = 2,
    .n_sprites = 1,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_4_actors),
    .triggers = TO_FAR_PTR_T(scene_4_triggers),
    .sprites = TO_FAR_PTR_T(scene_4_sprites),
    .script_init = TO_FAR_PTR_T(script_s4_init)
};
