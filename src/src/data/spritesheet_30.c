#pragma bank 255
// SpriteSheet: Merchant_F
  
#include "gbs_types.h"
#include "data/tileset_40.h"

BANKREF(spritesheet_30)

#define SPRITE_30_STATE_DEFAULT 0
#define SPRITE_30_STATE_SPECIAL_1 0

const metasprite_t spritesheet_30_metasprite_0[]  = {
    { 0, 8, 24, 3 }, { 0, -8, 26, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_1[]  = {
    { 0, 8, 28, 3 }, { 0, -8, 30, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_2[]  = {
    { 0, 8, 32, 3 }, { 0, -8, 34, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_3[]  = {
    { 0, 8, 0, 3 }, { 0, -8, 2, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_4[]  = {
    { 0, 8, 4, 3 }, { 0, -8, 6, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_5[]  = {
    { 0, 8, 8, 3 }, { 0, -8, 10, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_6[]  = {
    { 0, 8, 12, 3 }, { 0, -8, 14, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_7[]  = {
    { 0, 8, 16, 3 }, { 0, -8, 18, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_8[]  = {
    { 0, 8, 20, 3 }, { 0, -8, 22, 3 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_9[]  = {
    { 0, 0, 0, 35 }, { 0, 8, 2, 35 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_10[]  = {
    { 0, 0, 4, 35 }, { 0, 8, 6, 35 },
    {metasprite_end}
};

const metasprite_t spritesheet_30_metasprite_11[]  = {
    { 0, 0, 8, 35 }, { 0, 8, 10, 35 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_30_metasprites[] = {
    spritesheet_30_metasprite_0,
    spritesheet_30_metasprite_1,
    spritesheet_30_metasprite_0,
    spritesheet_30_metasprite_2,
    spritesheet_30_metasprite_3,
    spritesheet_30_metasprite_4,
    spritesheet_30_metasprite_3,
    spritesheet_30_metasprite_5,
    spritesheet_30_metasprite_6,
    spritesheet_30_metasprite_7,
    spritesheet_30_metasprite_6,
    spritesheet_30_metasprite_8,
    spritesheet_30_metasprite_9,
    spritesheet_30_metasprite_10,
    spritesheet_30_metasprite_9,
    spritesheet_30_metasprite_11
};

const struct animation_t spritesheet_30_animations[] = {
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 4,
        .end = 7
    },
    {
        .start = 8,
        .end = 11
    },
    {
        .start = 12,
        .end = 15
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 4,
        .end = 7
    },
    {
        .start = 8,
        .end = 11
    },
    {
        .start = 12,
        .end = 15
    }
};

const UWORD spritesheet_30_animations_lookup[] = {
    SPRITE_30_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_30 = {
    .n_metasprites = 16,
    .emote_origin = {
        .x = 0,
        .y = -16
    },
    .metasprites = spritesheet_30_metasprites,
    .animations = spritesheet_30_animations,
    .animations_lookup = spritesheet_30_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_40),
    .cgb_tileset = { NULL, NULL }
};
