#pragma bank 255

// Scene: D1_R3

#include "gbs_types.h"
#include "data/background_16.h"
#include "data/scene_16_collisions.h"
#include "data/palette_2.h"
#include "data/palette_11.h"
#include "data/spritesheet_4.h"
#include "data/scene_16_actors.h"
#include "data/scene_16_triggers.h"
#include "data/scene_16_sprites.h"
#include "data/script_s16_init.h"

BANKREF(scene_16)

const struct scene_t scene_16 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_16),
    .collisions = TO_FAR_PTR_T(scene_16_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_2),
    .sprite_palette = TO_FAR_PTR_T(palette_11),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 3,
    .n_triggers = 2,
    .n_sprites = 2,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_16_actors),
    .triggers = TO_FAR_PTR_T(scene_16_triggers),
    .sprites = TO_FAR_PTR_T(scene_16_sprites),
    .script_init = TO_FAR_PTR_T(script_s16_init)
};
