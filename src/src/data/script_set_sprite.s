.module script_set_sprite

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_set_sprite = 255
.globl ___bank_script_set_sprite
.CURRENT_SCRIPT_BANK == ___bank_script_set_sprite

_script_set_sprite::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 4)/
        VM_IF_CONST .EQ         .ARG0, 1, 1$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 4)/
        VM_IF_CONST .EQ         .ARG0, 2, 3$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 1, 5$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 2, 7$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 3, 9$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 4, 11$, 1
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_12, _spritesheet_12
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_12
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_12

        VM_JUMP                 12$
11$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_11, _spritesheet_11
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_11
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_11

12$:

        VM_JUMP                 10$
9$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_10, _spritesheet_10
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_10
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_10

10$:

        VM_JUMP                 8$
7$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_9, _spritesheet_9
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_9
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_9

8$:

        VM_JUMP                 6$
5$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_8, _spritesheet_8
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_8
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_8

6$:

        VM_JUMP                 4$
3$:
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 1, 13$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 2, 15$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 3, 17$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 4, 19$, 1
        VM_JUMP                 20$
19$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_7, _spritesheet_7
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_7
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_7

20$:

        VM_JUMP                 18$
17$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_6, _spritesheet_6
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_6
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_6

18$:

        VM_JUMP                 16$
15$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_5, _spritesheet_5
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_5
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_5

16$:

        VM_JUMP                 14$
13$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_4, _spritesheet_4
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_4
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_4

14$:

4$:

        VM_JUMP                 2$
1$:
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 1, 21$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 2, 23$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 4)/
        VM_IF_CONST .EQ         .ARG0, 3, 25$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_IF_CONST .EQ         .ARG0, 4, 27$, 1
        VM_JUMP                 28$
27$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_3, _spritesheet_3
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_3
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_3

28$:

        VM_JUMP                 26$
25$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_2, _spritesheet_2
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_2
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_2

26$:

        VM_JUMP                 24$
23$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_1, _spritesheet_1
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_1
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_1

24$:

        VM_JUMP                 22$
21$:
        ; Player Set Spritesheet
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_SPRITESHEET ACTOR, ___bank_spritesheet_0, _spritesheet_0
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_BANK, ___bank_spritesheet_0
        VM_SET_CONST            PLAYER_SPRITE_TOPDOWN_DATA, _spritesheet_0

22$:

2$:

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 15

        VM_POP                  4
        VM_RET_FAR
