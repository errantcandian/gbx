#pragma bank 255

// Scene: Choose Class - Thief
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_14.h"

BANKREF(scene_1_sprites)

const far_ptr_t scene_1_sprites[] = {
    TO_FAR_PTR_T(spritesheet_14)
};
