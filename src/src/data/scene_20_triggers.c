#pragma bank 255

// Scene: D1_R7
// Triggers

#include "gbs_types.h"
#include "data/script_s20t0_interact.h"
#include "data/script_s20t1_interact.h"

BANKREF(scene_20_triggers)

const struct trigger_t scene_20_triggers[] = {
    {
        // Trigger 1,
        .x = 1,
        .y = 7,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s20t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 18,
        .y = 9,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s20t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
