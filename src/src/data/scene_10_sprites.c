#pragma bank 255

// Scene: Pause - Combat
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_14.h"

BANKREF(scene_10_sprites)

const far_ptr_t scene_10_sprites[] = {
    TO_FAR_PTR_T(spritesheet_14)
};
