#ifndef SCENE_25_TRIGGERS_H
#define SCENE_25_TRIGGERS_H

// Scene: D1_R8
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_25_triggers)
extern const struct trigger_t scene_25_triggers[];

#endif
