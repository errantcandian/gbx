.module script_s7t0_interact

.include "vm.i"
.include "data/game_globals.i"
.include "macro.i"

.globl _fade_frames_per_step, ___bank_scene_2, _scene_2

.area _CODE_255

ACTOR = -4

___bank_script_s7t0_interact = 255
.globl ___bank_script_s7t0_interact
.CURRENT_SCRIPT_BANK == ___bank_script_s7t0_interact

_script_s7t0_interact::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Variable Set To False
        VM_SET_CONST            VAR_PC_GENDER, 0

        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 1024
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_RIGHT
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_2

        ; Stop Script
        VM_STOP
