#pragma bank 255

// Scene: Choose Gender - Thief
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_18.h"
#include "data/spritesheet_19.h"
#include "data/spritesheet_20.h"
#include "data/spritesheet_13.h"

BANKREF(scene_6_sprites)

const far_ptr_t scene_6_sprites[] = {
    TO_FAR_PTR_T(spritesheet_18),
    TO_FAR_PTR_T(spritesheet_19),
    TO_FAR_PTR_T(spritesheet_20),
    TO_FAR_PTR_T(spritesheet_13)
};
