#pragma bank 255

// Scene: Choose Gender - Cleric
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_21.h"
#include "data/spritesheet_22.h"
#include "data/spritesheet_23.h"
#include "data/spritesheet_13.h"

BANKREF(scene_7_sprites)

const far_ptr_t scene_7_sprites[] = {
    TO_FAR_PTR_T(spritesheet_21),
    TO_FAR_PTR_T(spritesheet_22),
    TO_FAR_PTR_T(spritesheet_23),
    TO_FAR_PTR_T(spritesheet_13)
};
