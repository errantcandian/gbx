#pragma bank 255
// SpriteSheet: goblin
  
#include "gbs_types.h"
#include "data/tileset_42.h"

BANKREF(spritesheet_32)

#define SPRITE_32_STATE_DEFAULT 0
#define SPRITE_32_STATE_SPECIAL_1 0

const metasprite_t spritesheet_32_metasprite_0[]  = {
    { 0, 16, 0, 4 }, { 0, -8, 2, 4 }, { -16, 0, 4, 4 }, { 16, -8, 6, 4 }, { -16, 0, 8, 4 }, { 16, -8, 10, 4 }, { -16, 0, 12, 4 },
    {metasprite_end}
};

const metasprite_t spritesheet_32_metasprite_1[]  = {
    { 0, 16, 14, 4 }, { 0, -8, 16, 4 }, { -16, 0, 4, 4 }, { 16, -8, 18, 4 }, { -16, 0, 8, 4 }, { 16, -8, 10, 4 }, { -16, 0, 12, 4 },
    {metasprite_end}
};

const metasprite_t spritesheet_32_metasprite_2[]  = {
    { 0, 16, 0, 4 }, { 0, -8, 2, 4 }, { -16, 0, 4, 4 }, { 16, -8, 20, 4 }, { -16, 0, 22, 4 }, { 16, -8, 24, 4 }, { -16, 0, 26, 4 },
    {metasprite_end}
};

const metasprite_t spritesheet_32_metasprite_3[]  = {
    { 0, 16, 0, 4 }, { 0, -8, 28, 4 }, { -16, 0, 4, 4 }, { 16, -8, 20, 4 }, { -16, 0, 22, 4 }, { 16, -8, 24, 4 }, { -16, 0, 30, 4 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_32_metasprites[] = {
    spritesheet_32_metasprite_0,
    spritesheet_32_metasprite_1,
    spritesheet_32_metasprite_0,
    spritesheet_32_metasprite_2,
    spritesheet_32_metasprite_1,
    spritesheet_32_metasprite_3
};

const struct animation_t spritesheet_32_animations[] = {
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 4,
        .end = 5
    },
    {
        .start = 4,
        .end = 5
    },
    {
        .start = 4,
        .end = 5
    },
    {
        .start = 4,
        .end = 5
    }
};

const UWORD spritesheet_32_animations_lookup[] = {
    SPRITE_32_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_32 = {
    .n_metasprites = 6,
    .emote_origin = {
        .x = 0,
        .y = -32
    },
    .metasprites = spritesheet_32_metasprites,
    .animations = spritesheet_32_animations,
    .animations_lookup = spritesheet_32_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_42),
    .cgb_tileset = { NULL, NULL }
};
