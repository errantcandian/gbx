#pragma bank 255

// Scene: D1_R7
// Actors

#include "gbs_types.h"
#include "data/spritesheet_39.h"
#include "data/script_s20a0_interact.h"

BANKREF(scene_20_actors)

const struct actor_t scene_20_actors[] = {
    {
        // GoblinKing,
        .pos = {
            .x = 72 * 16,
            .y = 40 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_39),
        .move_speed = 16,
        .anim_tick = 31,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s20a0_interact),
        .reserve_tiles = 0
    }
};
