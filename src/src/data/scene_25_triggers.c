#pragma bank 255

// Scene: D1_R8
// Triggers

#include "gbs_types.h"
#include "data/script_s25t0_interact.h"
#include "data/script_s25t1_interact.h"
#include "data/script_s25t2_interact.h"
#include "data/script_s25t3_interact.h"
#include "data/script_s25t4_interact.h"
#include "data/script_s25t5_interact.h"
#include "data/script_s25t6_interact.h"

BANKREF(scene_25_triggers)

const struct trigger_t scene_25_triggers[] = {
    {
        // Trigger 1,
        .x = 0,
        .y = 3,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s25t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 0,
        .y = 10,
        .width = 1,
        .height = 3,
        .script = TO_FAR_PTR_T(script_s25t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Pit Trap,
        .x = 2,
        .y = 9,
        .width = 2,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s25t2_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 4,
        .x = 9,
        .y = 5,
        .width = 1,
        .height = 4,
        .script = TO_FAR_PTR_T(script_s25t3_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 5,
        .x = 11,
        .y = 5,
        .width = 1,
        .height = 3,
        .script = TO_FAR_PTR_T(script_s25t4_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 6,
        .x = 7,
        .y = 2,
        .width = 2,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s25t5_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 7,
        .x = 10,
        .y = 11,
        .width = 2,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s25t6_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
