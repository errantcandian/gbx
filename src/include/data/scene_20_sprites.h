#ifndef SCENE_20_SPRITES_H
#define SCENE_20_SPRITES_H

// Scene: D1_R7
// Sprites

#include "gbs_types.h"

BANKREF_EXTERN(scene_20_sprites)
extern const far_ptr_t scene_20_sprites[];

#endif
