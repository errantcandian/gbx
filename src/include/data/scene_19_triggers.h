#ifndef SCENE_19_TRIGGERS_H
#define SCENE_19_TRIGGERS_H

// Scene: D1_R6
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_19_triggers)
extern const struct trigger_t scene_19_triggers[];

#endif
