.module script_s0t4_interact

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_s0t4_interact = 255
.globl ___bank_script_s0t4_interact
.CURRENT_SCRIPT_BANK == ___bank_script_s0t4_interact

_script_s0t4_interact::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Variable Set To False
        VM_SET_CONST            VAR_STR_SCORE, 0

        ; Variable Set To False
        VM_SET_CONST            VAR_DEX_SCORE, 0

        ; Variable Set To False
        VM_SET_CONST            VAR_CON_SCORE, 0

        ; Variable Set To False
        VM_SET_CONST            VAR_INT_SCORE, 0

        ; Variable Set To False
        VM_SET_CONST            VAR_WIS_SCORE, 0

        ; Variable Set To False
        VM_SET_CONST            VAR_CHA_SCORE, 0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Stop Script
        VM_STOP
