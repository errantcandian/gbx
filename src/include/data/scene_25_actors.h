#ifndef SCENE_25_ACTORS_H
#define SCENE_25_ACTORS_H

// Scene: D1_R8
// Actors

#include "gbs_types.h"

BANKREF_EXTERN(scene_25_actors)
extern const struct actor_t scene_25_actors[];

#endif
