.module script_s18_init

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step

.area _CODE_255

ACTOR = -4

___bank_script_s18_init = 255
.globl ___bank_script_s18_init
.CURRENT_SCRIPT_BANK == ___bank_script_s18_init

_script_s18_init::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation State
        VM_ACTOR_SET_ANIM_SET   ACTOR, STATE_SPECIAL_1

        ; If Variable True
        VM_IF_CONST .GT         VAR_S18A18_SIRENDEAD, 0, 1$, 0
        ; Variable Set To Value
        VM_SET_CONST            VAR_S18A18_SIREN_HP, 4

        VM_JUMP                 2$
1$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_ACTOR_SET_ANIM_FRAME ACTOR

2$:

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Collisions
        VM_ACTOR_SET_COLL_ENABLED ACTOR, 0

        ; If Variable True
        VM_IF_CONST .GT         VAR_S18A18_CHESTEXPLODE, 0, 3$, 0
        ; If Variable True
        VM_IF_CONST .GT         VAR_S18A18_CHESTOPEN, 0, 5$, 0
        VM_JUMP                 6$
5$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 1
        VM_ACTOR_SET_ANIM_FRAME ACTOR

6$:

        VM_JUMP                 4$
3$:
        ; Actor Hide
        VM_SET_CONST            ACTOR, 5
        VM_ACTOR_SET_HIDDEN     ACTOR, 1
        VM_ACTOR_DEACTIVATE     ACTOR

4$:

        VM_PUSH_CONST 0
        VM_PUSH_VALUE PLAYER_SPRITE_TOPDOWN_BANK
        VM_PUSH_VALUE PLAYER_SPRITE_TOPDOWN_DATA
        VM_ACTOR_SET_SPRITESHEET_BY_REF .ARG2, .ARG1

        ; Call Script: Set Sprite
        VM_PUSH_CONST           VAR_PC_CLASS ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_GENDER ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_set_sprite, _script_set_sprite
        VM_POP                  2

        ; Call Script: Return from Start Menu
        VM_PUSH_CONST           VAR_RETURNINGFROMSTARTMENU ; Variable .ARG4
        VM_PUSH_CONST           VAR_PC_Y ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_X ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_return_from_start_menu, _script_return_from_start_menu
        VM_POP                  3

        ; Variable Set To Value
        VM_SET_CONST            VAR_S18_ROOMNUM, 5

        ; Input Script Attach
        VM_CONTEXT_PREPARE      1, ___bank_script_input_41, _script_input_41
        VM_INPUT_ATTACH         128, ^/(1 | .OVERRIDE_DEFAULT)/

        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Stop Script
        VM_STOP
