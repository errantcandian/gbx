#pragma bank 255

// Scene: D1_R9

#include "gbs_types.h"
#include "data/background_22.h"
#include "data/scene_24_collisions.h"
#include "data/palette_0.h"
#include "data/palette_12.h"
#include "data/spritesheet_4.h"
#include "data/scene_24_actors.h"
#include "data/scene_24_triggers.h"
#include "data/scene_24_sprites.h"
#include "data/script_s24_init.h"

BANKREF(scene_24)

const struct scene_t scene_24 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_22),
    .collisions = TO_FAR_PTR_T(scene_24_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_0),
    .sprite_palette = TO_FAR_PTR_T(palette_12),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 1,
    .n_triggers = 2,
    .n_sprites = 1,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_24_actors),
    .triggers = TO_FAR_PTR_T(scene_24_triggers),
    .sprites = TO_FAR_PTR_T(scene_24_sprites),
    .script_init = TO_FAR_PTR_T(script_s24_init)
};
