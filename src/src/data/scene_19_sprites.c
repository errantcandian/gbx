#pragma bank 255

// Scene: D1_R6
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/spritesheet_38.h"

BANKREF(scene_19_sprites)

const far_ptr_t scene_19_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29),
    TO_FAR_PTR_T(spritesheet_38)
};
