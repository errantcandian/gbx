#pragma bank 255

// Scene: Dungeon1_Start
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/spritesheet_30.h"

BANKREF(scene_13_sprites)

const far_ptr_t scene_13_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29),
    TO_FAR_PTR_T(spritesheet_30)
};
