#pragma bank 255

// Scene: Generate Scores
// Triggers

#include "gbs_types.h"
#include "data/script_s0t0_interact.h"
#include "data/script_s0t1_interact.h"
#include "data/script_s0t2_interact.h"
#include "data/script_s0t3_interact.h"
#include "data/script_s0t4_interact.h"
#include "data/script_s0t5_interact.h"
#include "data/script_s0t6_interact.h"

BANKREF(scene_0_triggers)

const struct trigger_t scene_0_triggers[] = {
    {
        // 3d6,
        .x = 7,
        .y = 5,
        .width = 7,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s0t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // 4d6,
        .x = 7,
        .y = 7,
        .width = 12,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s0t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // d20,
        .x = 7,
        .y = 9,
        .width = 8,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s0t2_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Manual,
        .x = 7,
        .y = 11,
        .width = 10,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s0t3_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Reset,
        .x = 0,
        .y = 14,
        .width = 7,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s0t4_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Confirm,
        .x = 7,
        .y = 14,
        .width = 9,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s0t5_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Info,
        .x = 0,
        .y = 16,
        .width = 7,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s0t6_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
