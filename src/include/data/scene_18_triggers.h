#ifndef SCENE_18_TRIGGERS_H
#define SCENE_18_TRIGGERS_H

// Scene: D1_R5
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_18_triggers)
extern const struct trigger_t scene_18_triggers[];

#endif
