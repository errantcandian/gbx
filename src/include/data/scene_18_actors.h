#ifndef SCENE_18_ACTORS_H
#define SCENE_18_ACTORS_H

// Scene: D1_R5
// Actors

#include "gbs_types.h"

BANKREF_EXTERN(scene_18_actors)
extern const struct actor_t scene_18_actors[];

#endif
