#ifndef SCRIPT_SET_SPRITE_H
#define SCRIPT_SET_SPRITE_H

// Script script_set_sprite

#include "gbs_types.h"

BANKREF_EXTERN(script_set_sprite)
extern const unsigned char script_set_sprite[];

#endif
