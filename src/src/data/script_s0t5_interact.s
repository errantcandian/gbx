.module script_s0t5_interact

.include "vm.i"
.include "data/game_globals.i"
.include "macro.i"

.globl _fade_frames_per_step, ___bank_scene_3, _scene_3, b_camera_shake_frames, _camera_shake_frames

.area _CODE_255

ACTOR = -4

___bank_script_s0t5_interact = 255
.globl ___bank_script_s0t5_interact
.CURRENT_SCRIPT_BANK == ___bank_script_s0t5_interact

_script_s0t5_interact::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_STR_SCORE, 0, 1$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_DEX_SCORE, 0, 3$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_CON_SCORE, 0, 5$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_INT_SCORE, 0, 7$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_WIS_SCORE, 0, 9$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_CHA_SCORE, 0, 11$, 0
        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 1024
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_RIGHT
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_3

        VM_JUMP                 12$
11$:
        ; Camera Shake
        VM_PUSH_CONST           12
        VM_PUSH_CONST           ^/(.CAMERA_SHAKE_X | .CAMERA_SHAKE_Y)/
        VM_INVOKE               b_camera_shake_frames, _camera_shake_frames, 2, .ARG1
12$:

        VM_JUMP                 10$
9$:
        ; Camera Shake
        VM_PUSH_CONST           12
        VM_PUSH_CONST           ^/(.CAMERA_SHAKE_X | .CAMERA_SHAKE_Y)/
        VM_INVOKE               b_camera_shake_frames, _camera_shake_frames, 2, .ARG1
10$:

        VM_JUMP                 8$
7$:
        ; Camera Shake
        VM_PUSH_CONST           12
        VM_PUSH_CONST           ^/(.CAMERA_SHAKE_X | .CAMERA_SHAKE_Y)/
        VM_INVOKE               b_camera_shake_frames, _camera_shake_frames, 2, .ARG1
8$:

        VM_JUMP                 6$
5$:
        ; Camera Shake
        VM_PUSH_CONST           12
        VM_PUSH_CONST           ^/(.CAMERA_SHAKE_X | .CAMERA_SHAKE_Y)/
        VM_INVOKE               b_camera_shake_frames, _camera_shake_frames, 2, .ARG1
6$:

        VM_JUMP                 4$
3$:
        ; Camera Shake
        VM_PUSH_CONST           12
        VM_PUSH_CONST           ^/(.CAMERA_SHAKE_X | .CAMERA_SHAKE_Y)/
        VM_INVOKE               b_camera_shake_frames, _camera_shake_frames, 2, .ARG1
4$:

        VM_JUMP                 2$
1$:
        ; Camera Shake
        VM_PUSH_CONST           12
        VM_PUSH_CONST           ^/(.CAMERA_SHAKE_X | .CAMERA_SHAKE_Y)/
        VM_INVOKE               b_camera_shake_frames, _camera_shake_frames, 2, .ARG1
2$:

        ; Stop Script
        VM_STOP
