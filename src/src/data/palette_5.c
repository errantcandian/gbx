#pragma bank 255

// Palette: 5

#include "gbs_types.h"

BANKREF(palette_5)

const struct palette_t palette_5 = {
    .mask = 0xFF,
    .palette = {
        DMG_PALETTE(DMG_WHITE, DMG_LITE_GRAY, DMG_DARK_GRAY, DMG_BLACK)
    },
    .cgb_palette = {
        CGB_PALETTE(RGB(29, 31, 28), RGB(30, 2, 7), RGB(10, 19, 15), RGB(4, 5, 10)),
        CGB_PALETTE(RGB(29, 31, 28), RGB(30, 2, 7), RGB(10, 19, 15), RGB(4, 5, 10)),
        CGB_PALETTE(RGB(29, 31, 28), RGB(30, 2, 7), RGB(10, 19, 15), RGB(4, 5, 10)),
        CGB_PALETTE(RGB(29, 31, 28), RGB(30, 2, 7), RGB(10, 19, 15), RGB(4, 5, 10)),
        CGB_PALETTE(RGB(29, 31, 28), RGB(22, 30, 17), RGB(10, 19, 15), RGB(4, 5, 10)),
        CGB_PALETTE(RGB(29, 31, 28), RGB(22, 30, 17), RGB(10, 19, 15), RGB(4, 5, 10)),
        CGB_PALETTE(RGB(29, 31, 28), RGB(22, 30, 17), RGB(10, 19, 15), RGB(4, 5, 10)),
        CGB_PALETTE(RGB(29, 31, 28), RGB(22, 30, 17), RGB(10, 19, 15), RGB(4, 5, 10)) 
    }
};
