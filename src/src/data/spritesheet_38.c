#pragma bank 255
// SpriteSheet: GoblinWorker
  
#include "gbs_types.h"
#include "data/tileset_48.h"

BANKREF(spritesheet_38)

#define SPRITE_38_STATE_DEFAULT 0
#define SPRITE_38_STATE_SPECIAL_1 0

const metasprite_t spritesheet_38_metasprite_0[]  = {
    { 0, 16, 0, 4 }, { 0, -8, 2, 4 }, { 0, -8, 4, 4 },
    {metasprite_end}
};

const metasprite_t spritesheet_38_metasprite_1[]  = {
    { 0, 16, 6, 4 }, { 0, -8, 8, 4 }, { 0, -8, 4, 4 },
    {metasprite_end}
};

const metasprite_t spritesheet_38_metasprite_2[]  = {
    { 0, 7, 10, 4 }, { -16, 0, 12, 4 }, { 16, -8, 14, 4 }, { -16, 0, 16, 4 },
    {metasprite_end}
};

const metasprite_t spritesheet_38_metasprite_3[]  = {
    { 0, 6, 18, 4 }, { -16, 0, 20, 4 }, { 16, -8, 22, 4 }, { -16, 0, 24, 4 },
    {metasprite_end}
};

const metasprite_t spritesheet_38_metasprite_4[]  = {
    { 0, 15, 26, 4 }, { 0, -8, 28, 4 }, { 0, -8, 30, 4 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_38_metasprites[] = {
    spritesheet_38_metasprite_0,
    spritesheet_38_metasprite_1,
    spritesheet_38_metasprite_2,
    spritesheet_38_metasprite_3,
    spritesheet_38_metasprite_4
};

const struct animation_t spritesheet_38_animations[] = {
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    }
};

const UWORD spritesheet_38_animations_lookup[] = {
    SPRITE_38_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_38 = {
    .n_metasprites = 5,
    .emote_origin = {
        .x = 0,
        .y = -32
    },
    .metasprites = spritesheet_38_metasprites,
    .animations = spritesheet_38_animations,
    .animations_lookup = spritesheet_38_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_48),
    .cgb_tileset = { NULL, NULL }
};
