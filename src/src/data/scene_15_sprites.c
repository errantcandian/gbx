#pragma bank 255

// Scene: D1_R2
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/spritesheet_33.h"

BANKREF(scene_15_sprites)

const far_ptr_t scene_15_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29),
    TO_FAR_PTR_T(spritesheet_33)
};
