#pragma bank 255

// Scene: Choose Gender - Fighter
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_15.h"
#include "data/spritesheet_16.h"
#include "data/spritesheet_17.h"
#include "data/spritesheet_13.h"

BANKREF(scene_5_sprites)

const far_ptr_t scene_5_sprites[] = {
    TO_FAR_PTR_T(spritesheet_15),
    TO_FAR_PTR_T(spritesheet_16),
    TO_FAR_PTR_T(spritesheet_17),
    TO_FAR_PTR_T(spritesheet_13)
};
