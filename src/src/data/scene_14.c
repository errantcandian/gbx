#pragma bank 255

// Scene: D1_R1

#include "gbs_types.h"
#include "data/background_14.h"
#include "data/scene_14_collisions.h"
#include "data/palette_2.h"
#include "data/palette_9.h"
#include "data/spritesheet_4.h"
#include "data/scene_14_actors.h"
#include "data/scene_14_triggers.h"
#include "data/scene_14_sprites.h"
#include "data/script_s14_init.h"

BANKREF(scene_14)

const struct scene_t scene_14 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_14),
    .collisions = TO_FAR_PTR_T(scene_14_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_2),
    .sprite_palette = TO_FAR_PTR_T(palette_9),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 7,
    .n_triggers = 3,
    .n_sprites = 3,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_14_actors),
    .triggers = TO_FAR_PTR_T(scene_14_triggers),
    .sprites = TO_FAR_PTR_T(scene_14_sprites),
    .script_init = TO_FAR_PTR_T(script_s14_init)
};
