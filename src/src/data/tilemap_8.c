#pragma bank 255

// Tilemap 8

#include "gbs_types.h"

BANKREF(tilemap_8)

const unsigned char tilemap_8[] = {
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x2C, 0x2C, 0x2C, 0x03, 0x2D, 0x2D, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x2D, 0x2D, 0x03, 0x03, 0x2E, 0x2F, 0x0D, 0x30,
    0x31, 0x03, 0x03, 0x32, 0x03, 0x24, 0x1D, 0x33, 0x1D, 0x34, 0x03, 0x35, 0x03, 0x03, 0x32, 0x03,
    0x03, 0x36, 0x36, 0x36, 0x03, 0x37, 0x37, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x37, 0x37, 0x03, 0x03, 0x2E, 0x38, 0x06, 0x39, 0x31, 0x03, 0x03, 0x32, 0x03, 0x18, 0x1D, 0x1A,
    0x34, 0x15, 0x3A, 0x35, 0x03, 0x03, 0x32, 0x03, 0x03, 0x36, 0x36, 0x36, 0x03, 0x37, 0x37, 0x03,
    0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x3B, 0x3B, 0x3C, 0x03, 0x2E, 0x3D, 0x3E, 0x0C,
    0x31, 0x03, 0x03, 0x32, 0x04, 0x05, 0x2F, 0x3F, 0x09, 0x40, 0x40, 0x2F, 0x05, 0x41, 0x32, 0x03,
    0x03, 0x36, 0x36, 0x36, 0x03, 0x37, 0x37, 0x03, 0x0F, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10,
    0x10, 0x10, 0x42, 0x03, 0x2E, 0x09, 0x0C, 0x0D, 0x31, 0x03, 0x03, 0x32, 0x03, 0x43, 0x14, 0x21,
    0x44, 0x1A, 0x15, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x36, 0x36, 0x36, 0x03, 0x37, 0x37, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x2E, 0x45, 0x09, 0x2F,
    0x31, 0x03, 0x03, 0x32, 0x03, 0x46, 0x47, 0x2B, 0x34, 0x14, 0x1F, 0x1A, 0x15, 0x16, 0x14, 0x17,
    0x03, 0x36, 0x36, 0x36, 0x03, 0x37, 0x37, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x2E, 0x3D, 0x48, 0x49, 0x31, 0x03, 0x03, 0x32, 0x03, 0x19, 0x14, 0x1C,
    0x16, 0x1A, 0x34, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x4A, 0x4A, 0x4A, 0x03, 0x4B, 0x4B, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03
};
