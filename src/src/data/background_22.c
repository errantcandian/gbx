#pragma bank 255

// Background: Dungeon1_R9

#include "gbs_types.h"
#include "data/tileset_6.h"
#include "data/tilemap_22.h"
#include "data/tilemap_attr_5.h"

BANKREF(background_22)

const struct background_t background_22 = {
    .width = 20,
    .height = 18,
    .tileset = TO_FAR_PTR_T(tileset_6),
    .cgb_tileset = { NULL, NULL },
    .tilemap = TO_FAR_PTR_T(tilemap_22),
    .cgb_tilemap_attr = TO_FAR_PTR_T(tilemap_attr_5)
};
