.module script_scoregen_3d6

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255


___bank_script_scoregen_3d6 = 255
.globl ___bank_script_scoregen_3d6
.CURRENT_SCRIPT_BANK == ___bank_script_scoregen_3d6

_script_scoregen_3d6::
        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 6
        VM_SET_INDIRECT         ^/(.ARG2 - 1)/, .ARG0
        VM_POP                  1

        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 6
        VM_SET_INDIRECT         ^/(.ARG3 - 1)/, .ARG0
        VM_POP                  1

        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 6
        VM_SET_INDIRECT         ^/(.ARG4 - 1)/, .ARG0
        VM_POP                  1

        ; Variable .ARG5 = $V0$+$V1$+$V2$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG3)/
            .R_OPERATOR .ADD
            .R_REF_IND  ^/(.ARG4)/
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG5 - 1)/, .ARG0
        VM_POP                  1

        VM_RET_FAR
