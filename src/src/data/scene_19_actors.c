#pragma bank 255

// Scene: D1_R6
// Actors

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/script_s19a0_interact.h"
#include "data/spritesheet_29.h"
#include "data/script_s19a1_interact.h"
#include "data/spritesheet_38.h"
#include "data/script_s19a2_interact.h"

BANKREF(scene_19_actors)

const struct actor_t scene_19_actors[] = {
    {
        // DoorW,
        .pos = {
            .x = 0 * 16,
            .y = 72 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_RIGHT,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s19a0_interact),
        .reserve_tiles = 0
    },
    {
        // DoorE,
        .pos = {
            .x = 144 * 16,
            .y = 64 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_LEFT,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s19a1_interact),
        .reserve_tiles = 0
    },
    {
        // GoblinWorker,
        .pos = {
            .x = 88 * 16,
            .y = 112 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_38),
        .move_speed = 16,
        .anim_tick = 15,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s19a2_interact),
        .reserve_tiles = 0
    }
};
