.module script_input_4

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_input_4 = 255
.globl ___bank_script_input_4
.CURRENT_SCRIPT_BANK == ___bank_script_input_4

_script_input_4::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .GTE Value
        VM_IF_CONST .GTE        VAR_S0T3_SELECTED_SCORE, 18, 1$, 0
        ; If Variable .LTE Value
        VM_IF_CONST .LTE        VAR_S0T3_SELECTED_SCORE, 2, 3$, 0
        ; Variable L1 = VAR_S0T3_SELECTED_SCORE+1
        VM_RPN
            .R_REF      VAR_S0T3_SELECTED_SCORE
            .R_INT16    1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_S0T3_SELECTED_SCORE, .ARG0
        VM_POP                  1

        VM_JUMP                 4$
3$:
        ; Variable Set To Value
        VM_SET_CONST            VAR_S0T3_SELECTED_SCORE, 3

4$:

        VM_JUMP                 2$
1$:
        ; Variable Set To Value
        VM_SET_CONST            VAR_S0T3_SELECTED_SCORE, 3

2$:

        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    1
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 5$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    2
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 7$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    4
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 9$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    8
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 11$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    16
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 13$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    32
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 15$, 1
        VM_JUMP                 16$
15$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_S0T3_SELECTED_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

16$:

        VM_JUMP                 14$
13$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_S0T3_SELECTED_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

14$:

        VM_JUMP                 12$
11$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_S0T3_SELECTED_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

12$:

        VM_JUMP                 10$
9$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_S0T3_SELECTED_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

10$:

        VM_JUMP                 8$
7$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_S0T3_SELECTED_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

8$:

        VM_JUMP                 6$
5$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_S0T3_SELECTED_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

6$:

        ; Stop Script
        VM_STOP
