# GBX

Old-school dungeon crawling for the Game Boy Color, created in GB Studio.

## Playing the Game

You can play the game in-browser or download a compiled ROM here: https://errantcanadian.itch.io/gbx

Alternatively, you could download the source code from here and compile it locally.

Once you have the ROM, you can play it in your Game Boy Color emulator of choice, or you can flash it to a cartridge if you have the right equipment.

## Credits

This project includes assets and code from the following sources:
*     (https://chrismaltby.itch.io/gb-studio)
*     (https://sondanielson.itch.io/gameboy-simple-rpg-tileset)
*     (https://petricakegames.itch.io/retro-green-dungeon-tileset)
*     (https://monkeyimage.itch.io/world-tilesets-remastered)
*     (https://gumpyfunction.itch.io/game-boy-rpg-fantasy-tileset-free)
*     (https://gamekrazzy.itch.io/gameboy-zelda-like-dungeon-assets)
*     (https://niamov.itch.io/16x16-gb-tileset)
*     (https://elthen.itch.io/2d-pixel-art-goblin-worker-sprites)
*     (https://limezu.itch.io/weaponsprites)
*     (https://route1rodent.itch.io/16x16-rpg-character-sprite-sheet)
*     (https://opengameart.org/content/nes-style-rpg-characters)
*     (https://opengameart.org/content/more-nes-style-rpg-characters)
*     (https://github.com/crawl/tiles)
