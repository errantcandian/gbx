#pragma bank 255
// SpriteSheet: Cleric_F_selectScreen
  
#include "gbs_types.h"
#include "data/tileset_32.h"

BANKREF(spritesheet_22)

#define SPRITE_22_STATE_DEFAULT 0
#define SPRITE_22_STATE_SPECIAL_1 0

const metasprite_t spritesheet_22_metasprite_0[]  = {
    { 0, 8, 0, 0 }, { 0, -8, 2, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_22_metasprite_1[]  = {
    { 0, 8, 4, 0 }, { 0, -8, 6, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_22_metasprite_2[]  = {
    { 0, 8, 8, 0 }, { 0, -8, 10, 0 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_22_metasprites[] = {
    spritesheet_22_metasprite_0,
    spritesheet_22_metasprite_1,
    spritesheet_22_metasprite_0,
    spritesheet_22_metasprite_2
};

const struct animation_t spritesheet_22_animations[] = {
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    },
    {
        .start = 0,
        .end = 3
    }
};

const UWORD spritesheet_22_animations_lookup[] = {
    SPRITE_22_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_22 = {
    .n_metasprites = 4,
    .emote_origin = {
        .x = 0,
        .y = -16
    },
    .metasprites = spritesheet_22_metasprites,
    .animations = spritesheet_22_animations,
    .animations_lookup = spritesheet_22_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_32),
    .cgb_tileset = { NULL, NULL }
};
