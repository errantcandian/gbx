#ifndef SCENE_18_SPRITES_H
#define SCENE_18_SPRITES_H

// Scene: D1_R5
// Sprites

#include "gbs_types.h"

BANKREF_EXTERN(scene_18_sprites)
extern const far_ptr_t scene_18_sprites[];

#endif
