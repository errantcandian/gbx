.module script_return_from_start_menu

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_return_from_start_menu = 255
.globl ___bank_script_return_from_start_menu
.CURRENT_SCRIPT_BANK == ___bank_script_return_from_start_menu

_script_return_from_start_menu::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable True
        VM_PUSH_VALUE_IND       ^/(.ARG4 - 4)/
        VM_IF_CONST .GT         .ARG0, 0, 1$, 1
        VM_JUMP                 2$
1$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position To Variables
        VM_RPN
            .R_REF_IND  ^/(.ARG2 - 4)/
            .R_INT16    128
            .R_OPERATOR .MUL
            .R_REF_IND  ^/(.ARG3 - 4)/
            .R_INT16    128
            .R_OPERATOR .MUL
            .R_STOP
        VM_SET                  ^/(ACTOR + 1 - 2)/, .ARG1
        VM_SET                  ^/(ACTOR + 2 - 2)/, .ARG0
        VM_POP                  2
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To False
        VM_PUSH_CONST           0
        VM_SET_INDIRECT         ^/(.ARG4 - 5)/, .ARG0
        VM_POP                  1

2$:

        VM_POP                  4
        VM_RET_FAR
