#pragma bank 255

// Scene: D1_R4
// Triggers

#include "gbs_types.h"
#include "data/script_s17t0_interact.h"

BANKREF(scene_17_triggers)

const struct trigger_t scene_17_triggers[] = {
    {
        // Trigger 1,
        .x = 19,
        .y = 4,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s17t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
