#pragma bank 255

// Tileset: 32

#include "gbs_types.h"

BANKREF(tileset_32)

const struct tileset_t tileset_32 = {
    .n_tiles = 12,
    .tiles = {
        0xC0, 0xC0, 0x30, 0xF0, 0x08, 0xF8, 0x78, 0xF8, 0xC8, 0x78, 0xE8, 0x18, 0xFC, 0x24, 0xF8, 0x28,
        0xF0, 0x10, 0xE8, 0xF8, 0x88, 0xF8, 0x5C, 0xF4, 0x96, 0xFA, 0x1C, 0xFC, 0xB0, 0xD0, 0xE0, 0xA0,
        0x01, 0x01, 0x06, 0x07, 0x08, 0x0F, 0x0F, 0x0F, 0x09, 0x0F, 0x0B, 0x0C, 0x1F, 0x12, 0x0F, 0x0A,
        0x07, 0x04, 0x0B, 0x0F, 0x08, 0x0F, 0x1D, 0x17, 0x34, 0x2F, 0x1C, 0x1F, 0x06, 0x05, 0x03, 0x02,
        0x00, 0x00, 0xC0, 0xC0, 0x30, 0xF0, 0x08, 0xF8, 0x78, 0xF8, 0xC8, 0x78, 0xE8, 0x18, 0xFC, 0x24,
        0xF8, 0x28, 0xF0, 0x10, 0xE8, 0xF8, 0x98, 0xE8, 0x4C, 0xF4, 0x9C, 0xFC, 0xB0, 0xD0, 0xE0, 0xA0,
        0x00, 0x00, 0x01, 0x01, 0x06, 0x07, 0x08, 0x0F, 0x0F, 0x0F, 0x09, 0x0F, 0x0B, 0x0C, 0x1F, 0x12,
        0x0F, 0x0A, 0x07, 0x04, 0x0B, 0x0F, 0x0E, 0x0B, 0x0B, 0x0F, 0x1E, 0x13, 0x0F, 0x0E, 0x01, 0x01,
        0x00, 0x00, 0xC0, 0xC0, 0x30, 0xF0, 0x08, 0xF8, 0x78, 0xF8, 0xC8, 0x78, 0xE8, 0x18, 0xFC, 0x24,
        0xF8, 0x28, 0xF0, 0x10, 0xE8, 0xF8, 0xA8, 0xF8, 0x78, 0xF8, 0xAC, 0xF4, 0xF8, 0xB8, 0xC0, 0xC0,
        0x00, 0x00, 0x01, 0x01, 0x06, 0x07, 0x08, 0x0F, 0x0F, 0x0F, 0x09, 0x0F, 0x0B, 0x0C, 0x1F, 0x12,
        0x0F, 0x0A, 0x07, 0x04, 0x0B, 0x0F, 0x0C, 0x0B, 0x19, 0x17, 0x1C, 0x1F, 0x06, 0x05, 0x03, 0x02
    }
};
