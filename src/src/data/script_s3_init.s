.module script_s3_init

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step

.area _CODE_255

ACTOR = -4

___bank_script_s3_init = 255
.globl ___bank_script_s3_init
.CURRENT_SCRIPT_BANK == ___bank_script_s3_init

_script_s3_init::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_STR_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_DEX_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_CON_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_INT_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_WIS_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_CHA_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_STR_SCORE
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_STR_SCORE, .ARG0
        VM_POP                  1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_STR_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_CON_SCORE
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_CON_SCORE, .ARG0
        VM_POP                  1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_CON_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 1

        ; Input Script Remove
        VM_INPUT_DETACH         63

        ; Input Script Attach
        VM_CONTEXT_PREPARE      7, ___bank_script_input_17, _script_input_17
        VM_INPUT_ATTACH         2, ^/(7 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      8, ___bank_script_input_18, _script_input_18
        VM_INPUT_ATTACH         1, ^/(8 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      5, ___bank_script_input_13, _script_input_13
        VM_INPUT_ATTACH         8, ^/(5 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      6, ___bank_script_input_14, _script_input_14
        VM_INPUT_ATTACH         4, ^/(6 | .OVERRIDE_DEFAULT)/

        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Stop Script
        VM_STOP
