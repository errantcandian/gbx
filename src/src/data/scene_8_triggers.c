#pragma bank 255

// Scene: Choose Gender - Wizard
// Triggers

#include "gbs_types.h"
#include "data/script_s8t0_interact.h"
#include "data/script_s8t1_interact.h"
#include "data/script_s8t2_interact.h"
#include "data/script_s8t3_interact.h"
#include "data/script_s8t4_interact.h"

BANKREF(scene_8_triggers)

const struct trigger_t scene_8_triggers[] = {
    {
        // Trigger 1,
        .x = 0,
        .y = 16,
        .width = 9,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s8t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 10,
        .y = 16,
        .width = 10,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s8t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // SelectM,
        .x = 0,
        .y = 10,
        .width = 4,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s8t2_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // SelectNB,
        .x = 13,
        .y = 10,
        .width = 4,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s8t3_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // SelectF,
        .x = 7,
        .y = 10,
        .width = 4,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s8t4_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
