#pragma bank 255

// Scene: D1_R7

#include "gbs_types.h"
#include "data/background_20.h"
#include "data/scene_20_collisions.h"
#include "data/palette_2.h"
#include "data/palette_15.h"
#include "data/spritesheet_4.h"
#include "data/scene_20_actors.h"
#include "data/scene_20_triggers.h"
#include "data/scene_20_sprites.h"
#include "data/script_s20_init.h"

BANKREF(scene_20)

const struct scene_t scene_20 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_20),
    .collisions = TO_FAR_PTR_T(scene_20_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_2),
    .sprite_palette = TO_FAR_PTR_T(palette_15),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 1,
    .n_triggers = 2,
    .n_sprites = 1,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_20_actors),
    .triggers = TO_FAR_PTR_T(scene_20_triggers),
    .sprites = TO_FAR_PTR_T(scene_20_sprites),
    .script_init = TO_FAR_PTR_T(script_s20_init)
};
