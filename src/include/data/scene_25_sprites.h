#ifndef SCENE_25_SPRITES_H
#define SCENE_25_SPRITES_H

// Scene: D1_R8
// Sprites

#include "gbs_types.h"

BANKREF_EXTERN(scene_25_sprites)
extern const far_ptr_t scene_25_sprites[];

#endif
