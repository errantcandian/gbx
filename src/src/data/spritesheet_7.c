#pragma bank 255
// SpriteSheet: Wizard_F
  
#include "gbs_types.h"
#include "data/tileset_17.h"

BANKREF(spritesheet_7)

#define SPRITE_7_STATE_DEFAULT 0
#define SPRITE_7_STATE_SPECIAL_1 0

const metasprite_t spritesheet_7_metasprite_0[]  = {
    { 0, 8, 8, 0 }, { 0, -8, 10, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_1[]  = {
    { 0, 8, 0, 0 }, { 0, -8, 2, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_2[]  = {
    { 0, 8, 4, 0 }, { 0, -8, 6, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_3[]  = {
    { 0, 0, 0, 32 }, { 0, 8, 2, 32 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_4[]  = {
    { 0, 8, 28, 0 }, { 0, -8, 30, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_5[]  = {
    { 0, 8, 32, 0 }, { 0, -8, 34, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_6[]  = {
    { 0, 8, 12, 0 }, { 0, -8, 14, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_7[]  = {
    { 0, 8, 16, 0 }, { 0, -8, 18, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_8[]  = {
    { 0, 8, 20, 0 }, { 0, -8, 22, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_9[]  = {
    { 0, 8, 24, 0 }, { 0, -8, 26, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_10[]  = {
    { 0, 0, 12, 32 }, { 0, 8, 14, 32 },
    {metasprite_end}
};

const metasprite_t spritesheet_7_metasprite_11[]  = {
    { 0, 0, 16, 32 }, { 0, 8, 18, 32 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_7_metasprites[] = {
    spritesheet_7_metasprite_0,
    spritesheet_7_metasprite_1,
    spritesheet_7_metasprite_2,
    spritesheet_7_metasprite_3,
    spritesheet_7_metasprite_4,
    spritesheet_7_metasprite_0,
    spritesheet_7_metasprite_5,
    spritesheet_7_metasprite_0,
    spritesheet_7_metasprite_6,
    spritesheet_7_metasprite_1,
    spritesheet_7_metasprite_7,
    spritesheet_7_metasprite_1,
    spritesheet_7_metasprite_8,
    spritesheet_7_metasprite_2,
    spritesheet_7_metasprite_9,
    spritesheet_7_metasprite_2,
    spritesheet_7_metasprite_10,
    spritesheet_7_metasprite_3,
    spritesheet_7_metasprite_11,
    spritesheet_7_metasprite_3
};

const struct animation_t spritesheet_7_animations[] = {
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 1,
        .end = 1
    },
    {
        .start = 2,
        .end = 2
    },
    {
        .start = 3,
        .end = 3
    },
    {
        .start = 4,
        .end = 7
    },
    {
        .start = 8,
        .end = 11
    },
    {
        .start = 12,
        .end = 15
    },
    {
        .start = 16,
        .end = 19
    }
};

const UWORD spritesheet_7_animations_lookup[] = {
    SPRITE_7_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_7 = {
    .n_metasprites = 20,
    .emote_origin = {
        .x = 0,
        .y = -16
    },
    .metasprites = spritesheet_7_metasprites,
    .animations = spritesheet_7_animations,
    .animations_lookup = spritesheet_7_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_17),
    .cgb_tileset = { NULL, NULL }
};
