#pragma bank 255

// Palette: 13

#include "gbs_types.h"

BANKREF(palette_13)

const struct palette_t palette_13 = {
    .mask = 0xFF,
    .palette = {
        DMG_PALETTE(DMG_WHITE, DMG_WHITE, DMG_LITE_GRAY, DMG_BLACK),
        DMG_PALETTE(DMG_WHITE, DMG_WHITE, DMG_DARK_GRAY, DMG_BLACK)
    },
    .cgb_palette = {
        CGB_PALETTE(RGB(31, 30, 28), RGB(31, 30, 28), RGB(27, 16, 15), RGB(0, 0, 1)),
        CGB_PALETTE(RGB(31, 29, 25), RGB(31, 29, 25), RGB(27, 18, 9), RGB(6, 3, 10)),
        CGB_PALETTE(RGB(29, 29, 13), RGB(29, 29, 13), RGB(14, 12, 3), RGB(10, 8, 2)),
        CGB_PALETTE(RGB(0, 29, 27), RGB(0, 29, 27), RGB(0, 18, 31), RGB(1, 0, 17)),
        CGB_PALETTE(RGB(23, 26, 26), RGB(23, 26, 26), RGB(27, 16, 27), RGB(7, 0, 1)),
        CGB_PALETTE(RGB(31, 30, 28), RGB(31, 30, 28), RGB(27, 16, 15), RGB(0, 0, 1)),
        CGB_PALETTE(RGB(31, 30, 28), RGB(31, 30, 28), RGB(27, 16, 15), RGB(0, 0, 1)),
        CGB_PALETTE(RGB(31, 30, 28), RGB(31, 30, 28), RGB(27, 16, 15), RGB(0, 0, 1)) 
    }
};
