.module script_input_20

.include "vm.i"
.include "data/game_globals.i"
.include "macro.i"

.globl _fade_frames_per_step, ___bank_scene_3, _scene_3

.area _CODE_255

ACTOR = -4

___bank_script_input_20 = 255
.globl ___bank_script_input_20
.CURRENT_SCRIPT_BANK == ___bank_script_input_20

_script_input_20::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    1
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 1$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    4
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 3$, 1
        VM_JUMP                 4$
3$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 1408
        VM_SET_CONST            ^/(ACTOR + 2)/, 2048
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 2

4$:

        VM_JUMP                 2$
1$:
        ; Variable Decrement By 1
        VM_RPN
            .R_REF      VAR_INT_SCORE
            .R_INT8     1
            .R_OPERATOR .SUB
            .R_STOP
        VM_SET                  VAR_INT_SCORE, .ARG0
        VM_POP                  1

        ; Variable Decrement By 1
        VM_RPN
            .R_REF      VAR_WIS_SCORE
            .R_INT8     1
            .R_OPERATOR .SUB
            .R_STOP
        VM_SET                  VAR_WIS_SCORE, .ARG0
        VM_POP                  1

        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 1024
        VM_SET_CONST            ^/(ACTOR + 2)/, 128
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_RIGHT
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_3

2$:

        ; Stop Script
        VM_STOP
