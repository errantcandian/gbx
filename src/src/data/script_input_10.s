.module script_input_10

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_input_10 = 255
.globl ___bank_script_input_10
.CURRENT_SCRIPT_BANK == ___bank_script_input_10

_script_input_10::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    1
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 1$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    2
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 3$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    4
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 5$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    8
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 7$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_S0T3_CURRENT_ABILITY
            .R_INT16    16
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 9$, 1
        ; Variable Copy
        VM_SET                  VAR_CHA_SCORE, VAR_S0T3_SELECTED_SCORE

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 896
        VM_SET_CONST            ^/(ACTOR + 2)/, 1408
        VM_ACTOR_SET_POS        ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 8

        ; Input Script Remove
        VM_INPUT_DETACH         63

        ; Input Script Attach
        VM_CONTEXT_PREPARE      5, ___bank_script_input_8, _script_input_8
        VM_INPUT_ATTACH         8, ^/(5 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      6, ___bank_script_input_9, _script_input_9
        VM_INPUT_ATTACH         4, ^/(6 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      7, ___bank_script_input_3, _script_input_3
        VM_INPUT_ATTACH         2, ^/(7 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      8, ___bank_script_input_2, _script_input_2
        VM_INPUT_ATTACH         1, ^/(8 | .OVERRIDE_DEFAULT)/

        VM_JUMP                 10$
9$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 1408
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_S0T3_CURRENT_ABILITY, 32

        ; Variable Copy
        VM_SET                  VAR_WIS_SCORE, VAR_S0T3_SELECTED_SCORE

        VM_SET_CONST            ACTOR, 6
        ; Store Frame In Variable
        VM_ACTOR_GET_ANIM_FRAME ACTOR
        VM_SET                  VAR_S0T3_SELECTED_SCORE, ^/(ACTOR + 1)/

10$:

        VM_JUMP                 8$
7$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 1152
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_S0T3_CURRENT_ABILITY, 16

        ; Variable Copy
        VM_SET                  VAR_INT_SCORE, VAR_S0T3_SELECTED_SCORE

        VM_SET_CONST            ACTOR, 5
        ; Store Frame In Variable
        VM_ACTOR_GET_ANIM_FRAME ACTOR
        VM_SET                  VAR_S0T3_SELECTED_SCORE, ^/(ACTOR + 1)/

8$:

        VM_JUMP                 6$
5$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 896
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_S0T3_CURRENT_ABILITY, 8

        ; Variable Copy
        VM_SET                  VAR_CON_SCORE, VAR_S0T3_SELECTED_SCORE

        VM_SET_CONST            ACTOR, 4
        ; Store Frame In Variable
        VM_ACTOR_GET_ANIM_FRAME ACTOR
        VM_SET                  VAR_S0T3_SELECTED_SCORE, ^/(ACTOR + 1)/

6$:

        VM_JUMP                 4$
3$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 640
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_S0T3_CURRENT_ABILITY, 4

        ; Variable Copy
        VM_SET                  VAR_DEX_SCORE, VAR_S0T3_SELECTED_SCORE

        VM_SET_CONST            ACTOR, 3
        ; Store Frame In Variable
        VM_ACTOR_GET_ANIM_FRAME ACTOR
        VM_SET                  VAR_S0T3_SELECTED_SCORE, ^/(ACTOR + 1)/

4$:

        VM_JUMP                 2$
1$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 384
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_S0T3_CURRENT_ABILITY, 2

        ; Variable Copy
        VM_SET                  VAR_STR_SCORE, VAR_S0T3_SELECTED_SCORE

        VM_SET_CONST            ACTOR, 2
        ; Store Frame In Variable
        VM_ACTOR_GET_ANIM_FRAME ACTOR
        VM_SET                  VAR_S0T3_SELECTED_SCORE, ^/(ACTOR + 1)/

2$:

        ; Stop Script
        VM_STOP
