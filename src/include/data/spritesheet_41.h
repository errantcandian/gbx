#ifndef SPRITESHEET_41_H
#define SPRITESHEET_41_H

// SpriteSheet: 41

#include "gbs_types.h"

BANKREF_EXTERN(spritesheet_41)
extern const struct spritesheet_t spritesheet_41;

#endif
