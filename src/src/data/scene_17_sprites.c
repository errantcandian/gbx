#pragma bank 255

// Scene: D1_R4
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/spritesheet_35.h"

BANKREF(scene_17_sprites)

const far_ptr_t scene_17_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29),
    TO_FAR_PTR_T(spritesheet_35)
};
