#pragma bank 255
// SpriteSheet: Door
  
#include "gbs_types.h"
#include "data/tileset_39.h"

BANKREF(spritesheet_29)

#define SPRITE_29_STATE_DEFAULT 0
#define SPRITE_29_STATE_SPECIAL_1 0

const metasprite_t spritesheet_29_metasprite_0[]  = {
    { 0, 8, 8, 98 }, { 0, -8, 10, 98 },
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_1[]  = {
    { 0, 0, 8, 98 },
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_2[]  = {
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_3[]  = {
    { 0, 8, 0, 2 }, { 0, -8, 2, 2 },
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_4[]  = {
    { 0, 8, 4, 2 }, { 0, -8, 6, 2 },
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_5[]  = {
    { 0, 0, 8, 2 }, { 0, 8, 10, 2 },
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_6[]  = {
    { 0, 8, 8, 2 },
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_7[]  = {
    { 0, 0, 0, 34 }, { 0, 8, 2, 34 },
    {metasprite_end}
};

const metasprite_t spritesheet_29_metasprite_8[]  = {
    { 0, 0, 4, 34 }, { 0, 8, 6, 34 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_29_metasprites[] = {
    spritesheet_29_metasprite_0,
    spritesheet_29_metasprite_1,
    spritesheet_29_metasprite_2,
    spritesheet_29_metasprite_3,
    spritesheet_29_metasprite_4,
    spritesheet_29_metasprite_2,
    spritesheet_29_metasprite_5,
    spritesheet_29_metasprite_6,
    spritesheet_29_metasprite_2,
    spritesheet_29_metasprite_7,
    spritesheet_29_metasprite_8,
    spritesheet_29_metasprite_2
};

const struct animation_t spritesheet_29_animations[] = {
    {
        .start = 0,
        .end = 2
    },
    {
        .start = 3,
        .end = 5
    },
    {
        .start = 6,
        .end = 8
    },
    {
        .start = 9,
        .end = 11
    },
    {
        .start = 0,
        .end = 2
    },
    {
        .start = 3,
        .end = 5
    },
    {
        .start = 6,
        .end = 8
    },
    {
        .start = 9,
        .end = 11
    }
};

const UWORD spritesheet_29_animations_lookup[] = {
    SPRITE_29_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_29 = {
    .n_metasprites = 12,
    .emote_origin = {
        .x = 0,
        .y = -16
    },
    .metasprites = spritesheet_29_metasprites,
    .animations = spritesheet_29_animations,
    .animations_lookup = spritesheet_29_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_39),
    .cgb_tileset = { NULL, NULL }
};
