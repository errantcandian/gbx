.module script_s0t1_interact

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames

.area _CODE_255

ACTOR = -4

___bank_script_s0t1_interact = 255
.globl ___bank_script_s0t1_interact
.CURRENT_SCRIPT_BANK == ___bank_script_s0t1_interact

_script_s0t1_interact::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 1

        ; Call Script: ScoreGen_4d6
        VM_PUSH_CONST           VAR_STR_SCORE ; Variable .ARG6
        VM_PUSH_CONST           VAR_S0T1_FOURTH_D6 ; Variable .ARG5
        VM_PUSH_CONST           VAR_S0T1_THIRD_D6 ; Variable .ARG4
        VM_PUSH_CONST           VAR_S0T1_SECOND_D6 ; Variable .ARG3
        VM_PUSH_CONST           VAR_S0T1_FIRST_D6 ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_scoregen_4d6, _script_scoregen_4d6
        VM_POP                  5

        ; Call Script: ScoreGen_4d6
        VM_PUSH_CONST           VAR_DEX_SCORE ; Variable .ARG6
        VM_PUSH_CONST           VAR_S0T1_FOURTH_D6 ; Variable .ARG5
        VM_PUSH_CONST           VAR_S0T1_THIRD_D6 ; Variable .ARG4
        VM_PUSH_CONST           VAR_S0T1_SECOND_D6 ; Variable .ARG3
        VM_PUSH_CONST           VAR_S0T1_FIRST_D6 ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_scoregen_4d6, _script_scoregen_4d6
        VM_POP                  5

        ; Call Script: ScoreGen_4d6
        VM_PUSH_CONST           VAR_CON_SCORE ; Variable .ARG6
        VM_PUSH_CONST           VAR_S0T1_FOURTH_D6 ; Variable .ARG5
        VM_PUSH_CONST           VAR_S0T1_THIRD_D6 ; Variable .ARG4
        VM_PUSH_CONST           VAR_S0T1_SECOND_D6 ; Variable .ARG3
        VM_PUSH_CONST           VAR_S0T1_FIRST_D6 ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_scoregen_4d6, _script_scoregen_4d6
        VM_POP                  5

        ; Call Script: ScoreGen_4d6
        VM_PUSH_CONST           VAR_INT_SCORE ; Variable .ARG6
        VM_PUSH_CONST           VAR_S0T1_FOURTH_D6 ; Variable .ARG5
        VM_PUSH_CONST           VAR_S0T1_THIRD_D6 ; Variable .ARG4
        VM_PUSH_CONST           VAR_S0T1_SECOND_D6 ; Variable .ARG3
        VM_PUSH_CONST           VAR_S0T1_FIRST_D6 ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_scoregen_4d6, _script_scoregen_4d6
        VM_POP                  5

        ; Call Script: ScoreGen_4d6
        VM_PUSH_CONST           VAR_WIS_SCORE ; Variable .ARG6
        VM_PUSH_CONST           VAR_S0T1_FOURTH_D6 ; Variable .ARG5
        VM_PUSH_CONST           VAR_S0T1_THIRD_D6 ; Variable .ARG4
        VM_PUSH_CONST           VAR_S0T1_SECOND_D6 ; Variable .ARG3
        VM_PUSH_CONST           VAR_S0T1_FIRST_D6 ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_scoregen_4d6, _script_scoregen_4d6
        VM_POP                  5

        ; Call Script: ScoreGen_4d6
        VM_PUSH_CONST           VAR_CHA_SCORE ; Variable .ARG6
        VM_PUSH_CONST           VAR_S0T1_FOURTH_D6 ; Variable .ARG5
        VM_PUSH_CONST           VAR_S0T1_THIRD_D6 ; Variable .ARG4
        VM_PUSH_CONST           VAR_S0T1_SECOND_D6 ; Variable .ARG3
        VM_PUSH_CONST           VAR_S0T1_FIRST_D6 ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_scoregen_4d6, _script_scoregen_4d6
        VM_POP                  5

        ; Wait N Frames
        VM_PUSH_CONST           30
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_STR_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Wait N Frames
        VM_PUSH_CONST           30
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_DEX_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Wait N Frames
        VM_PUSH_CONST           30
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_CON_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Wait N Frames
        VM_PUSH_CONST           30
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_INT_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Wait N Frames
        VM_PUSH_CONST           30
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_WIS_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Wait N Frames
        VM_PUSH_CONST           30
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_CHA_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Stop Script
        VM_STOP
