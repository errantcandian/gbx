.module script_input_6

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255


___bank_script_input_6 = 255
.globl ___bank_script_input_6
.CURRENT_SCRIPT_BANK == ___bank_script_input_6

_script_input_6::
        ; Stop Script
        VM_STOP
