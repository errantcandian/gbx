#pragma bank 255

// Scene: D1_R8
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_35.h"
#include "data/spritesheet_42.h"

BANKREF(scene_25_sprites)

const far_ptr_t scene_25_sprites[] = {
    TO_FAR_PTR_T(spritesheet_35),
    TO_FAR_PTR_T(spritesheet_42)
};
