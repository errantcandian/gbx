#pragma bank 255

// Scene: D1_R9
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"

BANKREF(scene_24_sprites)

const far_ptr_t scene_24_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29)
};
