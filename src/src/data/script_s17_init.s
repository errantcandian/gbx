.module script_s17_init

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step

.area _CODE_255

ACTOR = -4

___bank_script_s17_init = 255
.globl ___bank_script_s17_init
.CURRENT_SCRIPT_BANK == ___bank_script_s17_init

_script_s17_init::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable True
        VM_IF_CONST .GT         VAR_S17A17_CHESTOPEN, 0, 1$, 0
        VM_JUMP                 2$
1$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 1
        VM_ACTOR_SET_ANIM_FRAME ACTOR

2$:

        ; Call Script: Set Sprite
        VM_PUSH_CONST           VAR_PC_CLASS ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_GENDER ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_set_sprite, _script_set_sprite
        VM_POP                  2

        ; If Variable True
        VM_IF_CONST .GT         VAR_S17_CAMETHRUDOOR, 0, 3$, 0
        ; Actor Hide
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_HIDDEN     ACTOR, 1
        VM_ACTOR_DEACTIVATE     ACTOR

        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Movement Speed
        VM_ACTOR_SET_MOVE_SPEED ACTOR, 48

        ; Actor Show
        VM_SET_CONST            ACTOR, 0
        VM_ACTOR_SET_HIDDEN     ACTOR, 0
        VM_ACTOR_ACTIVATE       ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Move To
        VM_SET_CONST            ^/(ACTOR + 1)/, 1152
        VM_SET_CONST            ^/(ACTOR + 2)/, 1024
        VM_SET_CONST            ^/(ACTOR + 3)/, 0
        VM_ACTOR_MOVE_TO        ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Movement Speed
        VM_ACTOR_SET_MOVE_SPEED ACTOR, 16

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 15

        ; Variable Set To True
        VM_SET_CONST            VAR_S17_CAMETHRUDOOR, 1

        VM_JUMP                 4$
3$:
        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_IN              1

4$:

        ; Call Script: Return from Start Menu
        VM_PUSH_CONST           VAR_RETURNINGFROMSTARTMENU ; Variable .ARG4
        VM_PUSH_CONST           VAR_PC_Y ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_X ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_return_from_start_menu, _script_return_from_start_menu
        VM_POP                  3

        ; Variable Set To Value
        VM_SET_CONST            VAR_S17_ROOMNUM, 4

        ; Input Script Attach
        VM_CONTEXT_PREPARE      1, ___bank_script_input_40, _script_input_40
        VM_INPUT_ATTACH         128, ^/(1 | .OVERRIDE_DEFAULT)/

        ; Stop Script
        VM_STOP
