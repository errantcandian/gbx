.module script_s16_init

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step

.area _CODE_255


___bank_script_s16_init = 255
.globl ___bank_script_s16_init
.CURRENT_SCRIPT_BANK == ___bank_script_s16_init

_script_s16_init::
        VM_LOCK

        VM_PUSH_CONST 0
        VM_PUSH_VALUE PLAYER_SPRITE_TOPDOWN_BANK
        VM_PUSH_VALUE PLAYER_SPRITE_TOPDOWN_DATA
        VM_ACTOR_SET_SPRITESHEET_BY_REF .ARG2, .ARG1

        ; Call Script: Set Sprite
        VM_PUSH_CONST           VAR_PC_CLASS ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_GENDER ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_set_sprite, _script_set_sprite
        VM_POP                  2

        ; Call Script: Return from Start Menu
        VM_PUSH_CONST           VAR_RETURNINGFROMSTARTMENU ; Variable .ARG4
        VM_PUSH_CONST           VAR_PC_Y ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_X ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_return_from_start_menu, _script_return_from_start_menu
        VM_POP                  3

        ; Variable Set To Value
        VM_SET_CONST            VAR_S16_ROOMNUM, 3

        ; Input Script Attach
        VM_CONTEXT_PREPARE      1, ___bank_script_input_39, _script_input_39
        VM_INPUT_ATTACH         128, ^/(1 | .OVERRIDE_DEFAULT)/

        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Stop Script
        VM_STOP
