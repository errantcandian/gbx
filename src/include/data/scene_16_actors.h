#ifndef SCENE_16_ACTORS_H
#define SCENE_16_ACTORS_H

// Scene: D1_R3
// Actors

#include "gbs_types.h"

BANKREF_EXTERN(scene_16_actors)
extern const struct actor_t scene_16_actors[];

#endif
