.module script_s17a1_interact

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_s17a1_interact = 255
.globl ___bank_script_s17a1_interact
.CURRENT_SCRIPT_BANK == ___bank_script_s17a1_interact

_script_s17a1_interact::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable True
        VM_IF_CONST .GT         VAR_S17A17_CHESTOPEN, 0, 1$, 0
        ; Variable Set To True
        VM_SET_CONST            VAR_S17A17_CHESTOPEN, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 1
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Text Dialogue
        VM_LOAD_TEXT            0
        .asciz "Inside the chest,\nyou find 500 G\nand some leather\narmour!"
        VM_OVERLAY_CLEAR        0, 0, 20, 6, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      0, 12, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT | .UI_WAIT_BTN_A)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/

        ; Variable 11 = VAR_PC_TREASURE+500
        VM_RPN
            .R_REF      VAR_PC_TREASURE
            .R_INT16    500
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_TREASURE, .ARG0
        VM_POP                  1

        ; If Variable .LT Value
        VM_IF_CONST .LT         VAR_EQUIP_ARMOUR, 1, 3$, 0
        ; Text Dialogue
        VM_LOAD_TEXT            0
        .asciz "Your armour is\nalready as good or\nbetter than this."
        VM_OVERLAY_CLEAR        0, 0, 20, 5, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      0, 13, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT | .UI_WAIT_BTN_A)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/

        ; Variable 11 = VAR_PC_TREASURE+25
        VM_RPN
            .R_REF      VAR_PC_TREASURE
            .R_INT16    25
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_TREASURE, .ARG0
        VM_POP                  1

        ; Text Dialogue
        VM_LOAD_TEXT            0
        .asciz "You decide to add\nthe armour to your\nloot. (+25 G)"
        VM_OVERLAY_CLEAR        0, 0, 20, 5, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      0, 13, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT | .UI_WAIT_BTN_A)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/

        VM_JUMP                 4$
3$:
        ; Variable Set To True
        VM_SET_CONST            VAR_EQUIP_ARMOUR, 1

        ; Text Dialogue
        VM_LOAD_TEXT            0
        .asciz "You equip the\narmour (+1)."
        VM_OVERLAY_CLEAR        0, 0, 20, 4, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      0, 14, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT | .UI_WAIT_BTN_A)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/

4$:

        VM_JUMP                 2$
1$:
2$:

        ; Stop Script
        VM_STOP
