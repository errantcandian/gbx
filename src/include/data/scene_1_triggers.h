#ifndef SCENE_1_TRIGGERS_H
#define SCENE_1_TRIGGERS_H

// Scene: Choose Class - Thief
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_1_triggers)
extern const struct trigger_t scene_1_triggers[];

#endif
