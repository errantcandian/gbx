#pragma bank 255
// SpriteSheet: numbers
  
#include "gbs_types.h"
#include "data/tileset_24.h"

BANKREF(spritesheet_14)

#define SPRITE_14_STATE_DEFAULT 0
#define SPRITE_14_STATE_SPECIAL_1 0

const metasprite_t spritesheet_14_metasprite_0[]  = {
    { 0, 8, 0, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_1[]  = {
    { 0, 8, 2, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_2[]  = {
    { 0, 8, 4, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_3[]  = {
    { 0, 8, 6, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_4[]  = {
    { 0, 8, 8, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_5[]  = {
    { 0, 8, 10, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_6[]  = {
    { 0, 8, 12, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_7[]  = {
    { 0, 8, 14, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_8[]  = {
    { 0, 8, 16, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_9[]  = {
    { 0, 8, 18, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_10[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 0, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_11[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 2, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_12[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 4, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_13[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 6, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_14[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 8, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_15[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 10, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_16[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 12, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_17[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 14, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_18[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 16, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_19[]  = {
    { 0, 0, 2, 0 }, { 0, 8, 18, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_20[]  = {
    { 0, 8, 0, 0 }, { 0, -8, 4, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_14_metasprite_21[]  = {
    { 0, 8, 20, 0 }, { 0, -8, 20, 0 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_14_metasprites[] = {
    spritesheet_14_metasprite_0,
    spritesheet_14_metasprite_1,
    spritesheet_14_metasprite_2,
    spritesheet_14_metasprite_3,
    spritesheet_14_metasprite_4,
    spritesheet_14_metasprite_5,
    spritesheet_14_metasprite_6,
    spritesheet_14_metasprite_7,
    spritesheet_14_metasprite_8,
    spritesheet_14_metasprite_9,
    spritesheet_14_metasprite_10,
    spritesheet_14_metasprite_11,
    spritesheet_14_metasprite_12,
    spritesheet_14_metasprite_13,
    spritesheet_14_metasprite_14,
    spritesheet_14_metasprite_15,
    spritesheet_14_metasprite_16,
    spritesheet_14_metasprite_17,
    spritesheet_14_metasprite_18,
    spritesheet_14_metasprite_19,
    spritesheet_14_metasprite_20,
    spritesheet_14_metasprite_21
};

const struct animation_t spritesheet_14_animations[] = {
    {
        .start = 0,
        .end = 21
    },
    {
        .start = 0,
        .end = 21
    },
    {
        .start = 0,
        .end = 21
    },
    {
        .start = 0,
        .end = 21
    },
    {
        .start = 0,
        .end = 21
    },
    {
        .start = 0,
        .end = 21
    },
    {
        .start = 0,
        .end = 21
    },
    {
        .start = 0,
        .end = 21
    }
};

const UWORD spritesheet_14_animations_lookup[] = {
    SPRITE_14_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_14 = {
    .n_metasprites = 22,
    .emote_origin = {
        .x = 0,
        .y = -16
    },
    .metasprites = spritesheet_14_metasprites,
    .animations = spritesheet_14_animations,
    .animations_lookup = spritesheet_14_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_24),
    .cgb_tileset = { NULL, NULL }
};
