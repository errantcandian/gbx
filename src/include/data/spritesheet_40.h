#ifndef SPRITESHEET_40_H
#define SPRITESHEET_40_H

// SpriteSheet: 40

#include "gbs_types.h"

BANKREF_EXTERN(spritesheet_40)
extern const struct spritesheet_t spritesheet_40;

#endif
