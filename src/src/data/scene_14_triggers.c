#pragma bank 255

// Scene: D1_R1
// Triggers

#include "gbs_types.h"
#include "data/script_s14t0_interact.h"
#include "data/script_s14t1_interact.h"
#include "data/script_s14t2_interact.h"

BANKREF(scene_14_triggers)

const struct trigger_t scene_14_triggers[] = {
    {
        // Trigger 1,
        .x = 0,
        .y = 8,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s14t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // D1_R1_to_Start,
        .x = 9,
        .y = 17,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s14t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 3,
        .x = 9,
        .y = 0,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s14t2_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
