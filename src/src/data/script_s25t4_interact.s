.module script_s25t4_interact

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_s25t4_interact = 255
.globl ___bank_script_s25t4_interact
.CURRENT_SCRIPT_BANK == ___bank_script_s25t4_interact

_script_s25t4_interact::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable True
        VM_IF_CONST .GT         VAR_MOVENOTRIGGER, 0, 1$, 0
        ; Variable Set To True
        VM_SET_CONST            VAR_MOVENOTRIGGER, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Movement Speed
        VM_ACTOR_SET_MOVE_SPEED ACTOR, 48

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Move To
        VM_SET_CONST            ^/(ACTOR + 1)/, 768
        VM_SET_CONST            ^/(ACTOR + 2)/, 896
        VM_SET_CONST            ^/(ACTOR + 3)/, .ACTOR_ATTR_H_FIRST
        VM_ACTOR_MOVE_TO        ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Movement Speed
        VM_ACTOR_SET_MOVE_SPEED ACTOR, 16

        ; Variable Set To False
        VM_SET_CONST            VAR_MOVENOTRIGGER, 0

        VM_JUMP                 2$
1$:
2$:

        ; Stop Script
        VM_STOP
