#pragma bank 255
// SpriteSheet: ClassButtonHeader
  
#include "gbs_types.h"
#include "data/tileset_37.h"

BANKREF(spritesheet_27)

#define SPRITE_27_STATE_DEFAULT 0
#define SPRITE_27_STATE_SPECIAL_1 0

const metasprite_t spritesheet_27_metasprite_0[]  = {
    {metasprite_end}
};

const metasprite_t spritesheet_27_metasprite_1[]  = {
    { -4, 40, 0, 1 }, { 0, -8, 2, 1 }, { 0, -8, 4, 1 }, { 0, -8, 6, 1 }, { 0, -8, 8, 1 }, { 0, -8, 10, 1 }, { 0, -8, 12, 1 }, { 0, -8, 14, 1 }, { 0, -8, 16, 1 }, { 0, -8, 18, 1 },
    {metasprite_end}
};

const metasprite_t spritesheet_27_metasprite_2[]  = {
    { -4, 40, 0, 1 }, { 0, -8, 2, 1 }, { 0, -8, 2, 1 }, { 0, -8, 16, 1 }, { 0, -8, 6, 1 }, { 0, -8, 14, 1 }, { 0, -8, 10, 1 }, { 0, -8, 8, 1 }, { 0, -8, 2, 1 }, { 0, -8, 18, 1 },
    {metasprite_end}
};

const metasprite_t spritesheet_27_metasprite_3[]  = {
    { -4, 40, 0, 1 }, { 0, -8, 2, 1 }, { 0, -8, 20, 1 }, { 0, -8, 14, 1 }, { 0, -8, 4, 1 }, { 0, -8, 6, 1 }, { 0, -8, 22, 1 }, { 0, -8, 20, 1 }, { 0, -8, 2, 1 }, { 0, -8, 18, 1 },
    {metasprite_end}
};

const metasprite_t spritesheet_27_metasprite_4[]  = {
    { -4, 40, 0, 1 }, { 0, -8, 2, 1 }, { 0, -8, 24, 1 }, { 0, -8, 4, 1 }, { 0, -8, 26, 1 }, { 0, -8, 28, 1 }, { 0, -8, 14, 1 }, { 0, -8, 30, 1 }, { 0, -8, 2, 1 }, { 0, -8, 18, 1 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_27_metasprites[] = {
    spritesheet_27_metasprite_0,
    spritesheet_27_metasprite_1,
    spritesheet_27_metasprite_2,
    spritesheet_27_metasprite_3,
    spritesheet_27_metasprite_4
};

const struct animation_t spritesheet_27_animations[] = {
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    }
};

const UWORD spritesheet_27_animations_lookup[] = {
    SPRITE_27_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_27 = {
    .n_metasprites = 5,
    .emote_origin = {
        .x = 0,
        .y = -24
    },
    .metasprites = spritesheet_27_metasprites,
    .animations = spritesheet_27_animations,
    .animations_lookup = spritesheet_27_animations_lookup,
    .bounds = {
        .left = -32,
        .bottom = 1,
        .right = 47,
        .top = -10
    },
    .tileset = TO_FAR_PTR_T(tileset_37),
    .cgb_tileset = { NULL, NULL }
};
