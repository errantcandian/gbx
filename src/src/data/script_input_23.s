.module script_input_23

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_input_23 = 255
.globl ___bank_script_input_23
.CURRENT_SCRIPT_BANK == ___bank_script_input_23

_script_input_23::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    1
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 1$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    2
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 3$, 1
        VM_JUMP                 4$
3$:
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    32
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 5$, 1
        VM_JUMP                 6$
5$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 2048
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 66

6$:

4$:

        VM_JUMP                 2$
1$:
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    16
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 7$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    8
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 9$, 1
        VM_JUMP                 10$
9$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 1280
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 5

10$:

        VM_JUMP                 8$
7$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 896
        VM_SET_CONST            ^/(ACTOR + 2)/, 1280
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 9

8$:

2$:

        ; Stop Script
        VM_STOP
