#ifndef SCENE_14_ACTORS_H
#define SCENE_14_ACTORS_H

// Scene: D1_R1
// Actors

#include "gbs_types.h"

BANKREF_EXTERN(scene_14_actors)
extern const struct actor_t scene_14_actors[];

#endif
