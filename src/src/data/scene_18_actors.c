#pragma bank 255

// Scene: D1_R5
// Actors

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/script_s18a0_interact.h"
#include "data/spritesheet_29.h"
#include "data/script_s18a1_interact.h"
#include "data/spritesheet_36.h"
#include "data/script_s18a2_interact.h"
#include "data/spritesheet_37.h"
#include "data/spritesheet_35.h"
#include "data/script_s18a4_interact.h"

BANKREF(scene_18_actors)

const struct actor_t scene_18_actors[] = {
    {
        // DoorW,
        .pos = {
            .x = 0 * 16,
            .y = 40 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_RIGHT,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s18a0_interact),
        .reserve_tiles = 0
    },
    {
        // DoorE,
        .pos = {
            .x = 144 * 16,
            .y = 72 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_LEFT,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s18a1_interact),
        .reserve_tiles = 0
    },
    {
        // Actor 3,
        .pos = {
            .x = 104 * 16,
            .y = 80 * 16
        },
        .bounds = {
            .left = -8,
            .bottom = 7,
            .right = 23,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_36),
        .move_speed = 16,
        .anim_tick = 63,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s18a2_interact),
        .reserve_tiles = 0
    },
    {
        // Actor 4,
        .pos = {
            .x = 104 * 16,
            .y = 96 * 16
        },
        .bounds = {
            .left = 8,
            .bottom = -16,
            .right = 8,
            .top = -16
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_37),
        .move_speed = 16,
        .anim_tick = 15,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .reserve_tiles = 0
    },
    {
        // Actor 5,
        .pos = {
            .x = 32 * 16,
            .y = 104 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_35),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s18a4_interact),
        .reserve_tiles = 0
    }
};
