#pragma bank 255

// Scene: D1_R5
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/spritesheet_36.h"
#include "data/spritesheet_37.h"
#include "data/spritesheet_35.h"

BANKREF(scene_18_sprites)

const far_ptr_t scene_18_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29),
    TO_FAR_PTR_T(spritesheet_36),
    TO_FAR_PTR_T(spritesheet_37),
    TO_FAR_PTR_T(spritesheet_35)
};
