.module script_start_menu

.include "vm.i"
.include "data/game_globals.i"
.include "macro.i"

.globl _fade_frames_per_step, ___bank_scene_22, _scene_22, ___bank_scene_9, _scene_9

.area _CODE_255

ACTOR = -4

___bank_script_start_menu = 255
.globl ___bank_script_start_menu
.CURRENT_SCRIPT_BANK == ___bank_script_start_menu

_script_start_menu::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Text Menu
        VM_PUSH_CONST           0
        VM_LOAD_TEXT            0
        .asciz "\001\001\003\003\002\002\001Skills\n\002\001Equip\n\002\001Spells\n\002\001Loot"
        VM_OVERLAY_CLEAR        0, 0, 10, 6, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      10, 18, .OVERLAY_SPEED_INSTANT
        VM_OVERLAY_MOVE_TO      10, 12, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/
        VM_CHOICE               .ARG0, .UI_MENU_CANCEL_B, 4
            .MENUITEM           1, 1, 1, 4, 0, 2
            .MENUITEM           1, 2, 1, 4, 1, 3
            .MENUITEM           1, 3, 1, 4, 2, 4
            .MENUITEM           1, 4, 1, 4, 3, 0
        VM_OVERLAY_MOVE_TO      10, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_SPEED_INSTANT
        VM_SET_INDIRECT         ^/(.ARG2 - 5)/, .ARG0
        VM_POP                  1

        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 4)/
        VM_IF_CONST .EQ         .ARG0, 1, 1$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 4)/
        VM_IF_CONST .EQ         .ARG0, 2, 3$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 4)/
        VM_IF_CONST .EQ         .ARG0, 3, 5$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG2 - 4)/
        VM_IF_CONST .EQ         .ARG0, 4, 7$, 1
        VM_JUMP                 8$
7$:
        ; Text Dialogue
        VM_PUSH_VALUE_IND       ^/(.ARG9 - 4)/
        VM_LOAD_TEXT            1
        .dw .ARG0
        .asciz "You currently have\n%d G\nworth of loot."
        VM_POP                  1
        VM_OVERLAY_CLEAR        0, 0, 20, 5, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      0, 13, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT | .UI_WAIT_BTN_A)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/

8$:

        VM_JUMP                 6$
5$:
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG6 - 4)/
        VM_IF_CONST .EQ         .ARG0, 3, 9$, 1
        ; If Variable .EQ Value
        VM_PUSH_VALUE_IND       ^/(.ARG6 - 4)/
        VM_IF_CONST .EQ         .ARG0, 4, 11$, 1
        ; Text Dialogue
        VM_LOAD_TEXT            0
        .asciz "You can't learn\nspells."
        VM_OVERLAY_CLEAR        0, 0, 20, 4, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      0, 14, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT | .UI_WAIT_BTN_A)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/

        VM_JUMP                 12$
11$:
        ; Text Menu
        VM_PUSH_CONST           0
        VM_LOAD_TEXT            0
        .asciz "\001\001\003\003\002\002\001Haste\n\002\001Protect"
        VM_OVERLAY_CLEAR        0, 0, 10, 4, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      10, 18, .OVERLAY_SPEED_INSTANT
        VM_OVERLAY_MOVE_TO      10, 14, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/
        VM_CHOICE               .ARG0, .UI_MENU_CANCEL_B, 2
            .MENUITEM           1, 1, 1, 2, 0, 2
            .MENUITEM           1, 2, 1, 2, 1, 0
        VM_OVERLAY_MOVE_TO      10, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_SPEED_INSTANT
        VM_SET_INDIRECT         ^/(.ARG7 - 5)/, .ARG0
        VM_POP                  1

12$:

        VM_JUMP                 10$
9$:
        ; Text Menu
        VM_PUSH_CONST           0
        VM_LOAD_TEXT            0
        .asciz "\001\001\003\003\002\002\001Heal\n\002\001Bless"
        VM_OVERLAY_CLEAR        0, 0, 10, 4, .UI_COLOR_WHITE, ^/(.UI_AUTO_SCROLL | .UI_DRAW_FRAME)/
        VM_OVERLAY_MOVE_TO      10, 18, .OVERLAY_SPEED_INSTANT
        VM_OVERLAY_MOVE_TO      10, 14, .OVERLAY_IN_SPEED
        VM_DISPLAY_TEXT
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/
        VM_CHOICE               .ARG0, .UI_MENU_CANCEL_B, 2
            .MENUITEM           1, 1, 1, 2, 0, 2
            .MENUITEM           1, 2, 1, 2, 1, 0
        VM_OVERLAY_MOVE_TO      10, 18, .OVERLAY_OUT_SPEED
        VM_OVERLAY_WAIT         .UI_MODAL, ^/(.UI_WAIT_WINDOW | .UI_WAIT_TEXT)/
        VM_OVERLAY_MOVE_TO      0, 18, .OVERLAY_SPEED_INSTANT
        VM_SET_INDIRECT         ^/(.ARG7 - 5)/, .ARG0
        VM_POP                  1

10$:

6$:

        VM_JUMP                 4$
3$:
        ; Variable Set To True
        VM_PUSH_CONST           1
        VM_SET_INDIRECT         ^/(.ARG11 - 5)/, .ARG0
        VM_POP                  1

        ; Variable Copy
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_SET_INDIRECT         ^/(.ARG8 - 5)/, .ARG0
        VM_POP                  1

        VM_SET_CONST            ACTOR, 0
        ; Store X Position In Variable
        VM_ACTOR_GET_POS        ACTOR
        VM_RPN
            .R_REF      ^/(ACTOR + 1)/
            .R_INT16    128
            .R_OPERATOR .DIV
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG10 - 5)/, .ARG0
        VM_POP                  1

        VM_SET_CONST            ACTOR, 0
        ; Store Y Position In Variable
        VM_ACTOR_GET_POS        ACTOR
        VM_RPN
            .R_REF      ^/(ACTOR + 2)/
            .R_INT16    128
            .R_OPERATOR .DIV
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG4 - 5)/, .ARG0
        VM_POP                  1

        ; Variable Set To Value
        VM_PUSH_CONST           1
        VM_SET_INDIRECT         ^/(.ARG5 - 5)/, .ARG0
        VM_POP                  1

        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 384
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_RIGHT
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_22

4$:

        VM_JUMP                 2$
1$:
        ; Variable Copy
        VM_PUSH_VALUE_IND       ^/(.ARG3 - 4)/
        VM_SET_INDIRECT         ^/(.ARG8 - 5)/, .ARG0
        VM_POP                  1

        VM_SET_CONST            ACTOR, 0
        ; Store X Position In Variable
        VM_ACTOR_GET_POS        ACTOR
        VM_RPN
            .R_REF      ^/(ACTOR + 1)/
            .R_INT16    128
            .R_OPERATOR .DIV
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG10 - 5)/, .ARG0
        VM_POP                  1

        VM_SET_CONST            ACTOR, 0
        ; Store Y Position In Variable
        VM_ACTOR_GET_POS        ACTOR
        VM_RPN
            .R_REF      ^/(ACTOR + 2)/
            .R_INT16    128
            .R_OPERATOR .DIV
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG4 - 5)/, .ARG0
        VM_POP                  1

        ; Variable Set To Value
        VM_PUSH_CONST           1
        VM_SET_INDIRECT         ^/(.ARG5 - 5)/, .ARG0
        VM_POP                  1

        ; Variable Set To True
        VM_PUSH_CONST           1
        VM_SET_INDIRECT         ^/(.ARG11 - 5)/, .ARG0
        VM_POP                  1

        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 896
        VM_SET_CONST            ^/(ACTOR + 2)/, 1152
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_RIGHT
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_9

2$:

        VM_POP                  4
        VM_RET_FAR
