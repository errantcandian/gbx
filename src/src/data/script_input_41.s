.module script_input_41

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255


___bank_script_input_41 = 255
.globl ___bank_script_input_41
.CURRENT_SCRIPT_BANK == ___bank_script_input_41

_script_input_41::
        ; Call Script: Start Menu
        VM_PUSH_CONST           VAR_RETURNINGFROMSTARTMENU ; Variable .ARG11
        VM_PUSH_CONST           VAR_PC_X ; Variable .ARG10
        VM_PUSH_CONST           VAR_PC_TREASURE ; Variable .ARG9
        VM_PUSH_CONST           VAR_PREVIOUSROOM ; Variable .ARG8
        VM_PUSH_CONST           VAR_TEMP_1 ; Variable .ARG7
        VM_PUSH_CONST           VAR_PC_CLASS ; Variable .ARG6
        VM_PUSH_CONST           VAR_CURSORPOS ; Variable .ARG5
        VM_PUSH_CONST           VAR_PC_Y ; Variable .ARG4
        VM_PUSH_CONST           VAR_S18_ROOMNUM ; Variable .ARG3
        VM_PUSH_CONST           VAR_TEMP_0 ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_start_menu, _script_start_menu
        VM_POP                  10

        ; Stop Script
        VM_STOP
