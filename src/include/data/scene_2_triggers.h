#ifndef SCENE_2_TRIGGERS_H
#define SCENE_2_TRIGGERS_H

// Scene: Choose Class - Cleric
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_2_triggers)
extern const struct trigger_t scene_2_triggers[];

#endif
