#ifndef SCENE_24_ACTORS_H
#define SCENE_24_ACTORS_H

// Scene: D1_R9
// Actors

#include "gbs_types.h"

BANKREF_EXTERN(scene_24_actors)
extern const struct actor_t scene_24_actors[];

#endif
