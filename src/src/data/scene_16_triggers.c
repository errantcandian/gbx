#pragma bank 255

// Scene: D1_R3
// Triggers

#include "gbs_types.h"
#include "data/script_s16t0_interact.h"
#include "data/script_s16t1_interact.h"

BANKREF(scene_16_triggers)

const struct trigger_t scene_16_triggers[] = {
    {
        // Trigger 1,
        .x = 9,
        .y = 17,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s16t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 9,
        .y = 9,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s16t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
