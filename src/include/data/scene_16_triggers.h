#ifndef SCENE_16_TRIGGERS_H
#define SCENE_16_TRIGGERS_H

// Scene: D1_R3
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_16_triggers)
extern const struct trigger_t scene_16_triggers[];

#endif
