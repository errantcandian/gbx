#pragma bank 255

// Scene: D1_R9
// Actors

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/script_s24a0_interact.h"

BANKREF(scene_24_actors)

const struct actor_t scene_24_actors[] = {
    {
        // DoorS,
        .pos = {
            .x = 80 * 16,
            .y = 136 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_UP,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s24a0_interact),
        .reserve_tiles = 0
    }
};
