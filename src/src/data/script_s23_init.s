.module script_s23_init

.include "vm.i"
.include "data/game_globals.i"
.include "macro.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step, ___bank_scene_21, _scene_21

.area _CODE_255

ACTOR = -4

___bank_script_s23_init = 255
.globl ___bank_script_s23_init
.CURRENT_SCRIPT_BANK == ___bank_script_s23_init

_script_s23_init::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        VM_LOAD_PALETTE         14, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 4, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 27, 30, 1, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 0, 1, 10, 19, 15, 4, 5, 10
        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         14, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 0, 1, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 4, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 27, 30, 1, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         14, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 0, 30, 29, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 0, 1, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 4, 31, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         14, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 4, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 0, 30, 29, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 0, 1, 31, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         15, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 2, 7, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 27, 30, 1, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 4, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 0, 30, 29, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         14, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 0, 1, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 27, 30, 1, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 4, 31, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         15, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 2, 7, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 0, 1, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 0, 1, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 27, 30, 1, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         15, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 2, 7, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 2, 7, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 0, 1, 31, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 30, 0, 1, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         12, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 2, 7, 10, 19, 15, 4, 5, 10
        .CGB_PAL                29, 31, 28, 0, 1, 31, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           6
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_LOAD_PALETTE         8, ^/(.PALETTE_BKG | .PALETTE_COMMIT)/
        .CGB_PAL                29, 31, 28, 30, 2, 7, 10, 19, 15, 4, 5, 10
        ; Wait N Frames
        VM_PUSH_CONST           60
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Wait N Frames
        VM_PUSH_CONST           60
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade Out
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1

        ; Load Scene
        VM_SET_CONST_INT8       _fade_frames_per_step, 3
        VM_FADE_OUT             1
        VM_SET_CONST            ACTOR, 0
        VM_SET_CONST            ^/(ACTOR + 1)/, 1152
        VM_SET_CONST            ^/(ACTOR + 2)/, 2048
        VM_ACTOR_SET_POS        ACTOR
        VM_ACTOR_SET_DIR        ACTOR, .DIR_DOWN
        VM_RAISE                EXCEPTION_CHANGE_SCENE, 3
            IMPORT_FAR_PTR_DATA _scene_21

        ; Stop Script
        VM_STOP
