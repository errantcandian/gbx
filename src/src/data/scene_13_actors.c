#pragma bank 255

// Scene: Dungeon1_Start
// Actors

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/script_s13a0_interact.h"
#include "data/spritesheet_30.h"
#include "data/script_s13a1_interact.h"

BANKREF(scene_13_actors)

const struct actor_t scene_13_actors[] = {
    {
        // DoorN,
        .pos = {
            .x = 72 * 16,
            .y = 8 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s13a0_interact),
        .reserve_tiles = 0
    },
    {
        // Merchant_D1_Start,
        .pos = {
            .x = 112 * 16,
            .y = 40 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_30),
        .move_speed = 16,
        .anim_tick = 15,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s13a1_interact),
        .reserve_tiles = 0
    }
};
