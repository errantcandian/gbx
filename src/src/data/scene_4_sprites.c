#pragma bank 255

// Scene: Choose Class - Wizard
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_14.h"

BANKREF(scene_4_sprites)

const far_ptr_t scene_4_sprites[] = {
    TO_FAR_PTR_T(spritesheet_14)
};
