#pragma bank 255
// SpriteSheet: SirenWell
  
#include "gbs_types.h"
#include "data/tileset_46.h"

BANKREF(spritesheet_36)

#define SPRITE_36_STATE_DEFAULT 0
#define SPRITE_36_STATE_SPECIAL_1 8

const metasprite_t spritesheet_36_metasprite_0[]  = {
    { 0, 9, 0, 3 }, { -16, 0, 2, 3 }, { 16, -8, 4, 3 }, { -16, 0, 6, 3 }, { 15, -8, 8, 3 }, { 1, 23, 10, 1 }, { 0, -8, 12, 1 }, { 0, -8, 14, 1 }, { 0, -8, 16, 1 },
    {metasprite_end}
};

const metasprite_t spritesheet_36_metasprite_1[]  = {
    { 0, 16, 10, 1 }, { 0, -8, 12, 1 }, { 0, -8, 14, 1 }, { 0, -8, 16, 1 },
    {metasprite_end}
};

const metasprite_t spritesheet_36_metasprite_2[]  = {
    { -9, 8, 18, 3 }, { 0, -8, 20, 3 }, { 9, 16, 10, 1 }, { 0, -8, 12, 1 }, { 0, -8, 14, 1 }, { 0, -8, 16, 1 },
    {metasprite_end}
};

const metasprite_t spritesheet_36_metasprite_3[]  = {
    { -8, 8, 2, 3 }, { 0, -8, 6, 3 }, { 8, 16, 10, 1 }, { 0, -8, 12, 1 }, { 0, -8, 14, 1 }, { 0, -8, 16, 1 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_36_metasprites[] = {
    spritesheet_36_metasprite_0,
    spritesheet_36_metasprite_1,
    spritesheet_36_metasprite_2,
    spritesheet_36_metasprite_3,
    spritesheet_36_metasprite_2
};

const struct animation_t spritesheet_36_animations[] = {
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 0,
        .end = 0
    },
    {
        .start = 1,
        .end = 4
    },
    {
        .start = 1,
        .end = 4
    },
    {
        .start = 1,
        .end = 4
    },
    {
        .start = 1,
        .end = 4
    },
    {
        .start = 1,
        .end = 4
    },
    {
        .start = 1,
        .end = 4
    },
    {
        .start = 1,
        .end = 4
    },
    {
        .start = 1,
        .end = 4
    }
};

const UWORD spritesheet_36_animations_lookup[] = {
    SPRITE_36_STATE_DEFAULT,
    SPRITE_36_STATE_SPECIAL_1
};

const struct spritesheet_t spritesheet_36 = {
    .n_metasprites = 5,
    .emote_origin = {
        .x = 0,
        .y = -32
    },
    .metasprites = spritesheet_36_metasprites,
    .animations = spritesheet_36_animations,
    .animations_lookup = spritesheet_36_animations_lookup,
    .bounds = {
        .left = -8,
        .bottom = 7,
        .right = 23,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_46),
    .cgb_tileset = { NULL, NULL }
};
