#pragma bank 255

// Scene: D1_R4
// Actors

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/script_s17a0_interact.h"
#include "data/spritesheet_35.h"
#include "data/script_s17a1_interact.h"

BANKREF(scene_17_actors)

const struct actor_t scene_17_actors[] = {
    {
        // DoorE,
        .pos = {
            .x = 144 * 16,
            .y = 40 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_LEFT,
        .sprite = TO_FAR_PTR_T(spritesheet_29),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s17a0_interact),
        .reserve_tiles = 0
    },
    {
        // Actor 2,
        .pos = {
            .x = 32 * 16,
            .y = 104 * 16
        },
        .bounds = {
            .left = 0,
            .bottom = 7,
            .right = 15,
            .top = -8
        },
        .dir = DIR_DOWN,
        .sprite = TO_FAR_PTR_T(spritesheet_35),
        .move_speed = 16,
        .anim_tick = 255,
        .pinned = FALSE,
        .collision_group = COLLISION_GROUP_NONE,
        .collision_enabled = TRUE,
        .script = TO_FAR_PTR_T(script_s17a1_interact),
        .reserve_tiles = 0
    }
};
