.module script_s13_init

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step

.area _CODE_255

ACTOR = -4

___bank_script_s13_init = 255
.globl ___bank_script_s13_init
.CURRENT_SCRIPT_BANK == ___bank_script_s13_init

_script_s13_init::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Call Script: Set Sprite
        VM_PUSH_CONST           VAR_PC_CLASS ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_GENDER ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_set_sprite, _script_set_sprite
        VM_POP                  2

        ; Call Script: Return from Start Menu
        VM_PUSH_CONST           VAR_RETURNINGFROMSTARTMENU ; Variable .ARG4
        VM_PUSH_CONST           VAR_PC_Y ; Variable .ARG3
        VM_PUSH_CONST           VAR_PC_X ; Variable .ARG2
        VM_CALL_FAR             ___bank_script_return_from_start_menu, _script_return_from_start_menu
        VM_POP                  3

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 15

        ; If Variable True
        VM_IF_CONST .GT         VAR_S13_RETURNINGTOSTART, 0, 1$, 0
        ; Variable Set To True
        VM_SET_CONST            VAR_PC_LEVEL, 1

        ; Variable Copy
        VM_SET                  VAR_PC_ATTACK, VAR_STR_SCORE

        ; Variable Set To True
        VM_SET_CONST            VAR_PC_DAMAGE, 1

        ; Variable Copy
        VM_SET                  VAR_PC_ARMOUR, VAR_CON_SCORE

        ; Variable Copy
        VM_SET                  VAR_PC_PERCEIVE, VAR_WIS_SCORE

        ; Variable Copy
        VM_SET                  VAR_PC_STEALTH, VAR_DEX_SCORE

        ; Variable Copy
        VM_SET                  VAR_PC_ESCAPE, VAR_DEX_SCORE

        ; Variable Copy
        VM_SET                  VAR_PC_FRIGHTEN, VAR_CHA_SCORE

        ; Variable Copy
        VM_SET                  VAR_PC_BEFRIEND, VAR_CHA_SCORE

        ; Variable Copy
        VM_SET                  VAR_PC_TRICK, VAR_CHA_SCORE

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_PC_CLASS, 1, 3$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_PC_CLASS, 2, 5$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_PC_CLASS, 3, 7$, 0
        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_PC_CLASS, 4, 9$, 0
        VM_JUMP                 10$
9$:
        ; Variable Set To Value
        VM_SET_CONST            VAR_PC_HP, 6

10$:

        VM_JUMP                 8$
7$:
        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_FRIGHTEN
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_FRIGHTEN, .ARG0
        VM_POP                  1

        ; Variable Set To Value
        VM_SET_CONST            VAR_PC_HP, 8

8$:

        VM_JUMP                 6$
5$:
        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_STEALTH
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_STEALTH, .ARG0
        VM_POP                  1

        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_ESCAPE
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_ESCAPE, .ARG0
        VM_POP                  1

        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_PERCEIVE
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_PERCEIVE, .ARG0
        VM_POP                  1

        ; Variable Set To Value
        VM_SET_CONST            VAR_PC_HP, 8

6$:

        VM_JUMP                 4$
3$:
        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_ATTACK
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_ATTACK, .ARG0
        VM_POP                  1

        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_DAMAGE
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_DAMAGE, .ARG0
        VM_POP                  1

        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_DAMAGE_TOTAL
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_DAMAGE_TOTAL, .ARG0
        VM_POP                  1

        ; Variable Increment By 1
        VM_RPN
            .R_REF      VAR_PC_ARMOUR
            .R_INT8     1
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_ARMOUR, .ARG0
        VM_POP                  1

        ; Variable Set To Value
        VM_SET_CONST            VAR_PC_HP, 10

4$:

        ; Variable 25 = VAR_PC_ARMOUR+VAR_EQUIP_ARMOUR
        VM_RPN
            .R_REF      VAR_PC_ARMOUR
            .R_REF      VAR_EQUIP_ARMOUR
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET                  VAR_PC_ARMOUR_TOTAL, .ARG0
        VM_POP                  1

        ; Variable Copy
        VM_SET                  VAR_PC_DAMAGE_TOTAL, VAR_PC_DAMAGE

        ; Variable Copy
        VM_SET                  VAR_PC_MAXHP, VAR_PC_HP

        ; Variable Set To True
        VM_SET_CONST            VAR_S13_RETURNINGTOSTART, 1

        VM_JUMP                 2$
1$:
2$:

        ; Variable Set To True
        VM_SET_CONST            VAR_S13_RETURNINGTOSTART, 1

        ; Variable Set To False
        VM_SET_CONST            VAR_S13_ROOMNUM, 0

        ; Input Script Attach
        VM_CONTEXT_PREPARE      1, ___bank_script_input_36, _script_input_36
        VM_INPUT_ATTACH         128, ^/(1 | .OVERRIDE_DEFAULT)/

        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Stop Script
        VM_STOP
