#pragma bank 255

// Scene: Choose Gender - Wizard

#include "gbs_types.h"
#include "data/background_7.h"
#include "data/scene_8_collisions.h"
#include "data/palette_1.h"
#include "data/palette_6.h"
#include "data/spritesheet_13.h"
#include "data/scene_8_actors.h"
#include "data/scene_8_triggers.h"
#include "data/scene_8_sprites.h"
#include "data/script_s8_init.h"

BANKREF(scene_8)

const struct scene_t scene_8 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_POINTNCLICK,
    .background = TO_FAR_PTR_T(background_7),
    .collisions = TO_FAR_PTR_T(scene_8_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_1),
    .sprite_palette = TO_FAR_PTR_T(palette_6),
    .reserve_tiles = 0,
    .player_sprite = TO_FAR_PTR_T(spritesheet_13),
    .n_actors = 4,
    .n_triggers = 5,
    .n_sprites = 4,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_8_actors),
    .triggers = TO_FAR_PTR_T(scene_8_triggers),
    .sprites = TO_FAR_PTR_T(scene_8_sprites),
    .script_init = TO_FAR_PTR_T(script_s8_init)
};
