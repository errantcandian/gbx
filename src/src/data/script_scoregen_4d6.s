.module script_scoregen_4d6

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255


___bank_script_scoregen_4d6 = 255
.globl ___bank_script_scoregen_4d6
.CURRENT_SCRIPT_BANK == ___bank_script_scoregen_4d6

_script_scoregen_4d6::
        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 6
        VM_SET_INDIRECT         ^/(.ARG2 - 1)/, .ARG0
        VM_POP                  1

        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 6
        VM_SET_INDIRECT         ^/(.ARG3 - 1)/, .ARG0
        VM_POP                  1

        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 6
        VM_SET_INDIRECT         ^/(.ARG4 - 1)/, .ARG0
        VM_POP                  1

        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 6
        VM_SET_INDIRECT         ^/(.ARG5 - 1)/, .ARG0
        VM_POP                  1

        ; If min($V0$,min($V1$,(min($V2$,$V3$))))==$V0$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG3)/
            .R_REF_IND  ^/(.ARG4)/
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_REF_IND  ^/(.ARG2)/
            .R_OPERATOR .EQ
            .R_STOP
        VM_IF_CONST .GT         .ARG0, 0, 1$, 1
        ; If min($V0$,min($V1$,(min($V2$,$V3$))))==$V1$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG3)/
            .R_REF_IND  ^/(.ARG4)/
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_REF_IND  ^/(.ARG3)/
            .R_OPERATOR .EQ
            .R_STOP
        VM_IF_CONST .GT         .ARG0, 0, 3$, 1
        ; If min($V0$,min($V1$,(min($V2$,$V3$))))==$V2$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG3)/
            .R_REF_IND  ^/(.ARG4)/
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_REF_IND  ^/(.ARG4)/
            .R_OPERATOR .EQ
            .R_STOP
        VM_IF_CONST .GT         .ARG0, 0, 5$, 1
        ; If min($V0$,min($V1$,(min($V2$,$V3$))))==$V3$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG3)/
            .R_REF_IND  ^/(.ARG4)/
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_OPERATOR .MIN
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .EQ
            .R_STOP
        VM_IF_CONST .GT         .ARG0, 0, 7$, 1
        ; Variable Set To False
        VM_PUSH_CONST           0
        VM_SET_INDIRECT         ^/(.ARG6 - 1)/, .ARG0
        VM_POP                  1

        VM_JUMP                 8$
7$:
        ; Variable .ARG6 = $V0$+$V1$+$V2$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG3)/
            .R_OPERATOR .ADD
            .R_REF_IND  ^/(.ARG4)/
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG6 - 1)/, .ARG0
        VM_POP                  1

8$:

        VM_JUMP                 6$
5$:
        ; Variable .ARG6 = $V0$+$V1$+$V3$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG3)/
            .R_OPERATOR .ADD
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG6 - 1)/, .ARG0
        VM_POP                  1

6$:

        VM_JUMP                 4$
3$:
        ; Variable .ARG6 = $V0$+$V2$+$V3$
        VM_RPN
            .R_REF_IND  ^/(.ARG2)/
            .R_REF_IND  ^/(.ARG4)/
            .R_OPERATOR .ADD
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG6 - 1)/, .ARG0
        VM_POP                  1

4$:

        VM_JUMP                 2$
1$:
        ; Variable .ARG6 = $V1$+$V2$+$V3$
        VM_RPN
            .R_REF_IND  ^/(.ARG3)/
            .R_REF_IND  ^/(.ARG4)/
            .R_OPERATOR .ADD
            .R_REF_IND  ^/(.ARG5)/
            .R_OPERATOR .ADD
            .R_STOP
        VM_SET_INDIRECT         ^/(.ARG6 - 1)/, .ARG0
        VM_POP                  1

2$:

        VM_RET_FAR
