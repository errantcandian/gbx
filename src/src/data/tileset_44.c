#pragma bank 255

// Tileset: 44

#include "gbs_types.h"

BANKREF(tileset_44)

const struct tileset_t tileset_44 = {
    .n_tiles = 0,
    .tiles = {
        0x00
    }
};
