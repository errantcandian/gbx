#pragma bank 255

// Scene: D1_R5

#include "gbs_types.h"
#include "data/background_18.h"
#include "data/scene_18_collisions.h"
#include "data/palette_4.h"
#include "data/palette_13.h"
#include "data/spritesheet_4.h"
#include "data/scene_18_actors.h"
#include "data/scene_18_triggers.h"
#include "data/scene_18_sprites.h"
#include "data/script_s18_init.h"

BANKREF(scene_18)

const struct scene_t scene_18 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_18),
    .collisions = TO_FAR_PTR_T(scene_18_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_4),
    .sprite_palette = TO_FAR_PTR_T(palette_13),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 5,
    .n_triggers = 2,
    .n_sprites = 4,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_18_actors),
    .triggers = TO_FAR_PTR_T(scene_18_triggers),
    .sprites = TO_FAR_PTR_T(scene_18_sprites),
    .script_init = TO_FAR_PTR_T(script_s18_init)
};
