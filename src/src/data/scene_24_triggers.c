#pragma bank 255

// Scene: D1_R9
// Triggers

#include "gbs_types.h"
#include "data/script_s24t0_interact.h"
#include "data/script_s24t1_interact.h"

BANKREF(scene_24_triggers)

const struct trigger_t scene_24_triggers[] = {
    {
        // Trigger 1,
        .x = 10,
        .y = 17,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s24t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 19,
        .y = 7,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s24t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
