#pragma bank 255
// SpriteSheet: goblinking
  
#include "gbs_types.h"
#include "data/tileset_49.h"

BANKREF(spritesheet_39)

#define SPRITE_39_STATE_DEFAULT 0
#define SPRITE_39_STATE_SPECIAL_1 0

const metasprite_t spritesheet_39_metasprite_0[]  = {
    { 0, 16, 0, 1 }, { -16, 0, 2, 1 }, { 16, -8, 4, 1 }, { -16, 0, 6, 1 }, { 16, -8, 8, 1 }, { -16, 0, 10, 1 }, { 16, -8, 12, 1 },
    {metasprite_end}
};

const metasprite_t spritesheet_39_metasprite_1[]  = {
    { 0, 16, 0, 1 }, { -16, 0, 14, 1 }, { 16, -8, 16, 1 }, { -16, 0, 18, 1 }, { 16, -8, 20, 1 }, { -16, 0, 22, 1 }, { 16, -8, 12, 1 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_39_metasprites[] = {
    spritesheet_39_metasprite_0,
    spritesheet_39_metasprite_1
};

const struct animation_t spritesheet_39_animations[] = {
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    },
    {
        .start = 0,
        .end = 1
    }
};

const UWORD spritesheet_39_animations_lookup[] = {
    SPRITE_39_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_39 = {
    .n_metasprites = 2,
    .emote_origin = {
        .x = 0,
        .y = -32
    },
    .metasprites = spritesheet_39_metasprites,
    .animations = spritesheet_39_animations,
    .animations_lookup = spritesheet_39_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_49),
    .cgb_tileset = { NULL, NULL }
};
