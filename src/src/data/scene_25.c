#pragma bank 255

// Scene: D1_R8

#include "gbs_types.h"
#include "data/background_21.h"
#include "data/scene_25_collisions.h"
#include "data/palette_0.h"
#include "data/palette_16.h"
#include "data/spritesheet_4.h"
#include "data/scene_25_actors.h"
#include "data/scene_25_triggers.h"
#include "data/scene_25_sprites.h"
#include "data/script_s25_init.h"

BANKREF(scene_25)

const struct scene_t scene_25 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_21),
    .collisions = TO_FAR_PTR_T(scene_25_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_0),
    .sprite_palette = TO_FAR_PTR_T(palette_16),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 4,
    .n_triggers = 7,
    .n_sprites = 2,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_25_actors),
    .triggers = TO_FAR_PTR_T(scene_25_triggers),
    .sprites = TO_FAR_PTR_T(scene_25_sprites),
    .script_init = TO_FAR_PTR_T(script_s25_init)
};
