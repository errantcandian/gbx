#pragma bank 255

// Scene: Choose Class - Cleric
// Triggers

#include "gbs_types.h"
#include "data/script_s2t0_interact.h"
#include "data/script_s2t1_interact.h"

BANKREF(scene_2_triggers)

const struct trigger_t scene_2_triggers[] = {
    {
        // CLR_GoBack,
        .x = 0,
        .y = 16,
        .width = 9,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s2t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // CLR_Confirm,
        .x = 10,
        .y = 16,
        .width = 10,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s2t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
