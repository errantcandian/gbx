.module script_s22_init

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step

.area _CODE_255

ACTOR = -4

___bank_script_s22_init = 255
.globl ___bank_script_s22_init
.CURRENT_SCRIPT_BANK == ___bank_script_s22_init

_script_s22_init::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_EQUIP_ARMOUR
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 8

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_EQUIP_ARMOUR
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 9

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_EQUIP_DAMAGE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 10

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_PC_CLASS
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Input Script Remove
        VM_INPUT_DETACH         255

        ; Input Script Attach
        VM_CONTEXT_PREPARE      5, ___bank_script_input_45, _script_input_45
        VM_INPUT_ATTACH         8, ^/(5 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      6, ___bank_script_input_46, _script_input_46
        VM_INPUT_ATTACH         4, ^/(6 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      1, ___bank_script_input_25, _script_input_25
        VM_INPUT_ATTACH         160, ^/(1 | .OVERRIDE_DEFAULT)/

        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Stop Script
        VM_STOP
