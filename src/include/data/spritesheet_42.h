#ifndef SPRITESHEET_42_H
#define SPRITESHEET_42_H

// SpriteSheet: 42

#include "gbs_types.h"

BANKREF_EXTERN(spritesheet_42)
extern const struct spritesheet_t spritesheet_42;

#endif
