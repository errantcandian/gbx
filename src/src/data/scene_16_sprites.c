#pragma bank 255

// Scene: D1_R3
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_29.h"
#include "data/spritesheet_31.h"

BANKREF(scene_16_sprites)

const far_ptr_t scene_16_sprites[] = {
    TO_FAR_PTR_T(spritesheet_29),
    TO_FAR_PTR_T(spritesheet_31)
};
