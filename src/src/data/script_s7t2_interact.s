.module script_s7t2_interact

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_s7t2_interact = 255
.globl ___bank_script_s7t2_interact
.CURRENT_SCRIPT_BANK == ___bank_script_s7t2_interact

_script_s7t2_interact::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; Variable Set To True
        VM_SET_CONST            VAR_PC_GENDER, 1

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 15

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 1280
        VM_SET_CONST            ^/(ACTOR + 2)/, 2048
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 34

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 128
        VM_SET_CONST            ^/(ACTOR + 2)/, 1280
        VM_ACTOR_SET_POS        ACTOR

        ; Actor Show
        VM_SET_CONST            ACTOR, 4
        VM_ACTOR_SET_HIDDEN     ACTOR, 0
        VM_ACTOR_ACTIVATE       ACTOR

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Stop Script
        VM_STOP
