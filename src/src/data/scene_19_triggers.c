#pragma bank 255

// Scene: D1_R6
// Triggers

#include "gbs_types.h"
#include "data/script_s19t0_interact.h"
#include "data/script_s19t1_interact.h"
#include "data/script_s19t2_interact.h"
#include "data/script_s19t3_interact.h"
#include "data/script_s19t4_interact.h"
#include "data/script_s19t5_interact.h"

BANKREF(scene_19_triggers)

const struct trigger_t scene_19_triggers[] = {
    {
        // Trigger 1,
        .x = 0,
        .y = 8,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s19t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 5,
        .y = 7,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s19t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 3,
        .x = 13,
        .y = 7,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s19t2_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 4,
        .x = 15,
        .y = 5,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s19t3_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 5,
        .x = 15,
        .y = 6,
        .width = 2,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s19t4_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 6,
        .x = 19,
        .y = 7,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s19t5_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
