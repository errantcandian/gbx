.module script_s0_init

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames, _fade_frames_per_step

.area _CODE_255

ACTOR = -4

___bank_script_s0_init = 255
.globl ___bank_script_s0_init
.CURRENT_SCRIPT_BANK == ___bank_script_s0_init

_script_s0_init::
        VM_LOCK

        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_STR_SCORE, 0, 1$, 0
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_STR_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        VM_JUMP                 2$
1$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 1

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

2$:

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_DEX_SCORE, 0, 3$, 0
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_DEX_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        VM_JUMP                 4$
3$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 2

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

4$:

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_CON_SCORE, 0, 5$, 0
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_CON_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        VM_JUMP                 6$
5$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 3

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

6$:

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_INT_SCORE, 0, 7$, 0
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_INT_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        VM_JUMP                 8$
7$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 4

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

8$:

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_WIS_SCORE, 0, 9$, 0
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_WIS_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        VM_JUMP                 10$
9$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 5

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

10$:

        ; If Variable .EQ Value
        VM_IF_CONST .EQ         VAR_CHA_SCORE, 0, 11$, 0
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Frame To Variable
        VM_SET                  ^/(ACTOR + 1)/, VAR_CHA_SCORE
        VM_ACTOR_SET_ANIM_FRAME ACTOR

        VM_JUMP                 12$
11$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 6

        ; Actor Set Animation Frame
        VM_SET_CONST            ^/(ACTOR + 1)/, 21
        VM_ACTOR_SET_ANIM_FRAME ACTOR

12$:

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Animation Tick
        VM_ACTOR_SET_ANIM_TICK  ACTOR, 255

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 1

        ; Input Script Attach
        VM_CONTEXT_PREPARE      5, ___bank_script_input_0, _script_input_0
        VM_INPUT_ATTACH         8, ^/(5 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      6, ___bank_script_input_1, _script_input_1
        VM_INPUT_ATTACH         4, ^/(6 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      8, ___bank_script_input_2, _script_input_2
        VM_INPUT_ATTACH         1, ^/(8 | .OVERRIDE_DEFAULT)/

        ; Input Script Attach
        VM_CONTEXT_PREPARE      7, ___bank_script_input_3, _script_input_3
        VM_INPUT_ATTACH         2, ^/(7 | .OVERRIDE_DEFAULT)/

        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; Fade In
        VM_SET_CONST_INT8       _fade_frames_per_step, 1
        VM_FADE_IN              1

        ; Stop Script
        VM_STOP
