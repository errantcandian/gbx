.module script_input_21

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_input_21 = 255
.globl ___bank_script_input_21
.CURRENT_SCRIPT_BANK == ___bank_script_input_21

_script_input_21::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    1
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 1$, 1
        VM_JUMP                 2$
1$:
        ; If Variable .NE Value
        VM_IF_CONST .NE         VAR_PC_GENDER, 0, 3$, 0
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 0
        VM_SET_CONST            ^/(ACTOR + 2)/, 2048
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 66

        VM_JUMP                 4$
3$:
        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 34

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 1280
        VM_SET_CONST            ^/(ACTOR + 2)/, 2048
        VM_ACTOR_SET_POS        ACTOR

4$:

2$:

        ; Stop Script
        VM_STOP
