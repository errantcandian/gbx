#pragma bank 255

// Scene: D1_R6

#include "gbs_types.h"
#include "data/background_19.h"
#include "data/scene_19_collisions.h"
#include "data/palette_0.h"
#include "data/palette_14.h"
#include "data/spritesheet_4.h"
#include "data/scene_19_actors.h"
#include "data/scene_19_triggers.h"
#include "data/scene_19_sprites.h"
#include "data/script_s19_init.h"

BANKREF(scene_19)

const struct scene_t scene_19 = {
    .width = 20,
    .height = 18,
    .type = SCENE_TYPE_TOPDOWN,
    .background = TO_FAR_PTR_T(background_19),
    .collisions = TO_FAR_PTR_T(scene_19_collisions),
    .parallax_rows = {
        PARALLAX_STEP(0,0,0)
    },
    .palette = TO_FAR_PTR_T(palette_0),
    .sprite_palette = TO_FAR_PTR_T(palette_14),
    .reserve_tiles = 36,
    .player_sprite = TO_FAR_PTR_T(spritesheet_4),
    .n_actors = 3,
    .n_triggers = 6,
    .n_sprites = 2,
    .n_projectiles = 0,
    .actors = TO_FAR_PTR_T(scene_19_actors),
    .triggers = TO_FAR_PTR_T(scene_19_triggers),
    .sprites = TO_FAR_PTR_T(scene_19_sprites),
    .script_init = TO_FAR_PTR_T(script_s19_init)
};
