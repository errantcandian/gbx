#pragma bank 255

// Scene: Choose Class - Wizard
// Triggers

#include "gbs_types.h"
#include "data/script_s4t0_interact.h"
#include "data/script_s4t1_interact.h"

BANKREF(scene_4_triggers)

const struct trigger_t scene_4_triggers[] = {
    {
        // WIZ_GoBack,
        .x = 0,
        .y = 16,
        .width = 9,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s4t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // WIZ_Confirm,
        .x = 10,
        .y = 16,
        .width = 10,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s4t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
