#pragma bank 255

// Tileset: 41

#include "gbs_types.h"

BANKREF(tileset_41)

const struct tileset_t tileset_41 = {
    .n_tiles = 16,
    .tiles = {
        0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0xFE, 0x3E, 0x5E, 0xA6, 0x2E, 0xD2, 0xAE, 0x52, 0xD6, 0x2A,
        0xD6, 0x2A, 0x96, 0x6A, 0x36, 0xEA, 0xEE, 0xD2, 0xDE, 0xA6, 0xFE, 0xFE, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x7E, 0x7D, 0x7A, 0x65, 0x75, 0x6A, 0x65, 0x5A, 0x6B, 0x54,
        0x6B, 0x54, 0x69, 0x56, 0x6C, 0x77, 0x77, 0x6B, 0x7B, 0x65, 0x7F, 0x7F, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0xFE, 0x3E, 0x6E, 0x96, 0xAE, 0x52, 0xAE, 0x52, 0xD6, 0x2A,
        0xD6, 0x2A, 0x96, 0x6A, 0x36, 0xEA, 0xEE, 0xD2, 0xDE, 0xA6, 0xFE, 0xFE, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x01, 0x00, 0x02, 0x01, 0x7E, 0x7D, 0x75, 0x6A, 0x75, 0x6A, 0x6B, 0x54, 0x6B, 0x54,
        0x6B, 0x54, 0x69, 0x56, 0x6C, 0x77, 0x77, 0x6B, 0x7B, 0x65, 0x7F, 0x7F, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x80, 0x00, 0xC0, 0x00, 0x7E, 0xBE, 0xAE, 0x56, 0xB6, 0x4A, 0xD6, 0x2A, 0xD6, 0x2A,
        0xD6, 0x2A, 0x96, 0x6A, 0x36, 0xEA, 0xEE, 0xD2, 0xDE, 0xA6, 0xFE, 0xFE, 0x00, 0x00, 0x00, 0x00,
        0x03, 0x00, 0x02, 0x01, 0x01, 0x00, 0x7F, 0x7E, 0x7A, 0x65, 0x76, 0x69, 0x65, 0x5A, 0x6B, 0x54,
        0x6B, 0x54, 0x69, 0x56, 0x6C, 0x77, 0x77, 0x6B, 0x7B, 0x65, 0x7F, 0x7F, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0xFE, 0x3E, 0x5E, 0xA6, 0xAE, 0x52, 0xAE, 0x52, 0xD6, 0x2A,
        0xD6, 0x2A, 0x96, 0x6A, 0x36, 0xEA, 0xEE, 0xD2, 0xDE, 0xA6, 0xFE, 0xFE, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x01, 0x00, 0x02, 0x01, 0x7E, 0x7D, 0x75, 0x6A, 0x65, 0x7A, 0x6B, 0x54, 0x6B, 0x54,
        0x6B, 0x54, 0x69, 0x56, 0x6C, 0x77, 0x77, 0x6B, 0x7B, 0x65, 0x7F, 0x7F, 0x00, 0x00, 0x00, 0x00
    }
};
