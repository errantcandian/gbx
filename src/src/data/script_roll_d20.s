.module script_roll_d20

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255


___bank_script_roll_d20 = 255
.globl ___bank_script_roll_d20
.CURRENT_SCRIPT_BANK == ___bank_script_roll_d20

_script_roll_d20::
        ; Variable Set To Random
        VM_PUSH_CONST           0
        VM_RAND                 .ARG0, 1, 20
        VM_SET_INDIRECT         ^/(.ARG2 - 1)/, .ARG0
        VM_POP                  1

        VM_RET_FAR
