#pragma bank 255

// Scene: Pause Menu - Skills
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_14.h"
#include "data/spritesheet_27.h"
#include "data/spritesheet_28.h"

BANKREF(scene_9_sprites)

const far_ptr_t scene_9_sprites[] = {
    TO_FAR_PTR_T(spritesheet_14),
    TO_FAR_PTR_T(spritesheet_27),
    TO_FAR_PTR_T(spritesheet_28)
};
