#pragma bank 255

// Scene: Choose Class - Thief
// Triggers

#include "gbs_types.h"
#include "data/script_s1t0_interact.h"
#include "data/script_s1t1_interact.h"

BANKREF(scene_1_triggers)

const struct trigger_t scene_1_triggers[] = {
    {
        // THF_GoBack,
        .x = 0,
        .y = 16,
        .width = 9,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s1t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // THF_Confirm,
        .x = 10,
        .y = 16,
        .width = 10,
        .height = 1,
        .script = TO_FAR_PTR_T(script_s1t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
