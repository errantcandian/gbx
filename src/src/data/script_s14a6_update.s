.module script_s14a6_update

.include "vm.i"
.include "data/game_globals.i"

.globl b_wait_frames, _wait_frames

.area _CODE_255

ACTOR = -4

___bank_script_s14a6_update = 255
.globl ___bank_script_s14a6_update
.CURRENT_SCRIPT_BANK == ___bank_script_s14a6_update

_script_s14a6_update::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

1$:
3$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 7

        ; Actor Emote
        VM_ACTOR_EMOTE          ACTOR, ___bank_emote_4, _emote_4

        ; Wait N Frames
        VM_PUSH_CONST           30
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        ; If Variable True
        VM_IF_CONST .GT         VAR_GOBLINACTIVE, 0, 4$, 0
        VM_JUMP                 5$
4$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 7

        ; Actor Emote
        VM_ACTOR_EMOTE          ACTOR, ___bank_emote_3, _emote_3

        ; Actor Set Active
        VM_SET_CONST            ACTOR, 7

        ; Actor Stop Update Script
        VM_ACTOR_TERMINATE_UPDATE ACTOR

5$:

        VM_JUMP                 3$
        ; Wait 1 Frame
        VM_PUSH_CONST           1
        VM_INVOKE               b_wait_frames, _wait_frames, 1, .ARG0

        VM_JUMP                 1$
        ; Stop Script
        VM_STOP
