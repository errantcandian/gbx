#ifndef SCENE_17_TRIGGERS_H
#define SCENE_17_TRIGGERS_H

// Scene: D1_R4
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_17_triggers)
extern const struct trigger_t scene_17_triggers[];

#endif
