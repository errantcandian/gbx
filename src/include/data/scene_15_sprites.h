#ifndef SCENE_15_SPRITES_H
#define SCENE_15_SPRITES_H

// Scene: D1_R2
// Sprites

#include "gbs_types.h"

BANKREF_EXTERN(scene_15_sprites)
extern const far_ptr_t scene_15_sprites[];

#endif
