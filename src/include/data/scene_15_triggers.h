#ifndef SCENE_15_TRIGGERS_H
#define SCENE_15_TRIGGERS_H

// Scene: D1_R2
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_15_triggers)
extern const struct trigger_t scene_15_triggers[];

#endif
