#pragma bank 255

// Scene: D1_R7
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_39.h"

BANKREF(scene_20_sprites)

const far_ptr_t scene_20_sprites[] = {
    TO_FAR_PTR_T(spritesheet_39)
};
