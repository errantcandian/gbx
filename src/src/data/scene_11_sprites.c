#pragma bank 255

// Scene: Pause - Explore
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_14.h"

BANKREF(scene_11_sprites)

const far_ptr_t scene_11_sprites[] = {
    TO_FAR_PTR_T(spritesheet_14)
};
