#pragma bank 255

// Scene: D1_R5
// Triggers

#include "gbs_types.h"
#include "data/script_s18t0_interact.h"
#include "data/script_s18t1_interact.h"

BANKREF(scene_18_triggers)

const struct trigger_t scene_18_triggers[] = {
    {
        // Trigger 1,
        .x = 0,
        .y = 4,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s18t0_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    },
    {
        // Trigger 2,
        .x = 19,
        .y = 8,
        .width = 1,
        .height = 2,
        .script = TO_FAR_PTR_T(script_s18t1_interact),
        .script_flags = TRIGGER_HAS_ENTER_SCRIPT
    }
};
