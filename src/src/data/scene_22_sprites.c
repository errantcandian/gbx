#pragma bank 255

// Scene: Pause - Equip
// Sprites

#include "gbs_types.h"
#include "data/spritesheet_14.h"
#include "data/spritesheet_40.h"
#include "data/spritesheet_41.h"
#include "data/spritesheet_28.h"

BANKREF(scene_22_sprites)

const far_ptr_t scene_22_sprites[] = {
    TO_FAR_PTR_T(spritesheet_14),
    TO_FAR_PTR_T(spritesheet_40),
    TO_FAR_PTR_T(spritesheet_41),
    TO_FAR_PTR_T(spritesheet_28)
};
