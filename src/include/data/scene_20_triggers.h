#ifndef SCENE_20_TRIGGERS_H
#define SCENE_20_TRIGGERS_H

// Scene: D1_R7
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_20_triggers)
extern const struct trigger_t scene_20_triggers[];

#endif
