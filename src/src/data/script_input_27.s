.module script_input_27

.include "vm.i"
.include "data/game_globals.i"

.area _CODE_255

ACTOR = -4

___bank_script_input_27 = 255
.globl ___bank_script_input_27
.CURRENT_SCRIPT_BANK == ___bank_script_input_27

_script_input_27::
        ; Local Actor
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0
        VM_PUSH_CONST           0

        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    4
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 1$, 1
        ; If Variable .B_AND Value
        VM_RPN
            .R_REF      VAR_CURSORPOS
            .R_INT16    2
            .R_OPERATOR .B_AND
            .R_STOP
        VM_IF_CONST .NE         .ARG0, 0, 3$, 1
        VM_JUMP                 4$
3$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 896
        VM_SET_CONST            ^/(ACTOR + 2)/, 1152
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 1

4$:

        VM_JUMP                 2$
1$:
        ; Actor Set Active
        VM_SET_CONST            ACTOR, 0

        ; Actor Set Position
        VM_SET_CONST            ^/(ACTOR + 1)/, 896
        VM_SET_CONST            ^/(ACTOR + 2)/, 1408
        VM_ACTOR_SET_POS        ACTOR

        ; Variable Set To Value
        VM_SET_CONST            VAR_CURSORPOS, 2

2$:

        ; Stop Script
        VM_STOP
