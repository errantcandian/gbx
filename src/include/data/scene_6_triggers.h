#ifndef SCENE_6_TRIGGERS_H
#define SCENE_6_TRIGGERS_H

// Scene: Choose Gender - Thief
// Triggers

#include "gbs_types.h"

BANKREF_EXTERN(scene_6_triggers)
extern const struct trigger_t scene_6_triggers[];

#endif
