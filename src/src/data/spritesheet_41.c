#pragma bank 255
// SpriteSheet: weapons
  
#include "gbs_types.h"
#include "data/tileset_51.h"

BANKREF(spritesheet_41)

#define SPRITE_41_STATE_DEFAULT 0
#define SPRITE_41_STATE_SPECIAL_1 0

const metasprite_t spritesheet_41_metasprite_0[]  = {
    { 0, 0, 0, 0 }, { 0, -8, 2, 0 }, { 0, -8, 2, 0 }, { 0, -8, 4, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_41_metasprite_1[]  = {
    { 0, 0, 6, 0 }, { 0, -8, 8, 0 }, { 0, -8, 2, 0 }, { 0, -8, 10, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_41_metasprite_2[]  = {
    { 0, 16, 6, 0 }, { 0, -8, 12, 0 }, { 0, -8, 8, 0 }, { 0, -8, 2, 0 }, { 0, -8, 14, 0 }, { 0, -8, 10, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_41_metasprite_3[]  = {
    { 0, 0, 8, 0 }, { 0, -8, 2, 0 }, { 0, -8, 14, 0 }, { 0, -8, 16, 0 },
    {metasprite_end}
};

const metasprite_t spritesheet_41_metasprite_4[]  = {
    { 0, 32, 8, 0 }, { 0, -8, 18, 0 }, { 0, -8, 20, 0 }, { 0, -8, 0, 0 }, { 0, -8, 20, 0 }, { 0, -8, 22, 0 }, { 0, -8, 24, 0 }, { 0, -8, 26, 0 },
    {metasprite_end}
};

const metasprite_t * const spritesheet_41_metasprites[] = {
    spritesheet_41_metasprite_0,
    spritesheet_41_metasprite_1,
    spritesheet_41_metasprite_2,
    spritesheet_41_metasprite_3,
    spritesheet_41_metasprite_4
};

const struct animation_t spritesheet_41_animations[] = {
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    },
    {
        .start = 0,
        .end = 4
    }
};

const UWORD spritesheet_41_animations_lookup[] = {
    SPRITE_41_STATE_DEFAULT
};

const struct spritesheet_t spritesheet_41 = {
    .n_metasprites = 5,
    .emote_origin = {
        .x = 0,
        .y = -32
    },
    .metasprites = spritesheet_41_metasprites,
    .animations = spritesheet_41_animations,
    .animations_lookup = spritesheet_41_animations_lookup,
    .bounds = {
        .left = 0,
        .bottom = 7,
        .right = 15,
        .top = -8
    },
    .tileset = TO_FAR_PTR_T(tileset_51),
    .cgb_tileset = { NULL, NULL }
};
